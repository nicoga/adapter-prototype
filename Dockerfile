FROM registry01.payulatam.com:5000/payu/java8

ARG APP_JAR

ENV APP_JAR ${APP_JAR}

USER root
RUN  useradd -l -u 5000 testadapter
COPY docker/opt /tmp
COPY target/${APP_JAR} /tmp/docker
RUN  mv /tmp/docker /opt/
RUN chown -R testadapter:testadapter /opt/docker/
WORKDIR /opt/docker
RUN chmod +x bin/run_container
USER testadapter
