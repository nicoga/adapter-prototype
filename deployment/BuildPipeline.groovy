/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */

#!/usr/bin/env groovy
mavenDockerBuildPlugin {
    name = "test-adapter"
    gitUrl = "https://bitbucket.org/pagosonline/test-adapter.git"
    email = "softwarearchitecture@payulatam.com"
    environment = [
            "HOUSTON_HOME": "/usr/local/payu/houston/"
    ]
    credentials = ["POL_ITERACIONES", "POL_KEY", "POL_SAL"]
}
