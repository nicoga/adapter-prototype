/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */

#!/usr/bin/env groovy
def houston = houstonCluster.instance
dockerServicePlugin {
    cluster = houston
    name = "test-adapter"
    stack = "houston"
    proxy = [
            "path"       : "test-adapter",
            "port"       : 18080,
            "rewritePath": true
    ]
    configs = [
            [
                    name     : "test-adapter-config",
                    extension: "yaml"
            ]
    ]
    secrets = [
            [
                    name: "test-adapter-secrets"
            ]
    ]
    environment = [
            "default"      : [
                    "RUN_JAVA_OPTS=-Xms128m -Xmx512m -Xss512k"
            ],
            "0-dev-local"  : [
                    "ENVIRONMENT=Local"
            ],
            "1-dev-cluster": [
                    "ENVIRONMENT=Dev-Cluster"
            ],
            "2-laptops"    : [
                    "ENVIRONMENT=Laptops"
            ],
            "3-qa1"        : [
                    "ENVIRONMENT=QA1"
            ],
            "3-qa10"       : [
                    "ENVIRONMENT=QA10"
            ],
            "4-load"       : [
                    "ENVIRONMENT=LOAD"
            ],
            "5-production" : [
                    "ENVIRONMENT=PRD",
                    "RUN_JAVA_OPTS= -Xms128m -Xmx512m -Xss512k " +
                            "-javaagent:/opt/docker/newrelic/newrelic.jar " +
                            "-Dnewrelic.config.file=/opt/docker/conf/newrelic.yml " +
                            "-Dnewrelic.environment=test-adapter-production"
            ],
            "6-aws-qa"     : [
                    "ENVIRONMENT=6-AWS-QA",
                    "RUN_JAVA_OPTS= -Xms128m -Xmx512m -Xss512k -XX:NativeMemoryTracking=summary"
            ]
    ]
}
