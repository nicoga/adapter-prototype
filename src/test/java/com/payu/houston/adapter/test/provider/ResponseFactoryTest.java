/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseCode;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseCode;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseMessage;
import com.payu.houston.adapter.queue.model.common.AmountDto;
import com.payu.houston.adapter.queue.model.common.OrderDto;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseCode;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidResponseCode;
import com.payu.houston.adapter.queue.model.voids.VoidResponseMessage;
import com.payu.houston.adapter.test.model.NetworkResponse;
import com.payu.houston.adapter.test.model.constant.ParametersName;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Test cases for the {@link ResponseFactory} class.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class ResponseFactoryTest {

	/**
	 * Test that method {@linkplain ResponseFactory#createAuthorizationResponseMessage} successfully creates the authorization response message.
	 */
	@Test
	public void createAuthorizationResponseMessage() {

		final AuthorizationRequestMessage authorizationRequestMessage = AuthorizationRequestMessage
				.withCorrelationId("123456").withOrder(
						OrderDto.withId(1234455).build())
				.withAmount(AmountDto.of("COP", new BigDecimal(200000)).build())
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567890123").build();

		final NetworkResponse networkResponse = NetworkResponse.builder().amount("200000").orderId("11111")
				.responseCode("0")
				.transactionDate(new Date().toString()).responseMessage("Approved transaction").state("ACCEPTED")
				.transactionCode("AUTHORIZED").build();

		ObjectMapper oMapper = new ObjectMapper();
		final AuthorizationResponseMessage expected = AuthorizationResponseMessage.withCorrelationId("123456")
				.withAuthorizationCode("ACCEPTED")
				.withTraceabilityId("11111")
				.withMessage("Approved transaction")
				.withAcquirerResponseCode("AUTHORIZED")
				.withResponseCode(AuthorizationResponseCode.APPROVED)
				.withResponseData(oMapper.convertValue(networkResponse, Map.class))
				.build();

		AuthorizationResponseMessage actual = ResponseFactory
				.createAuthorizationResponseMessage(authorizationRequestMessage, networkResponse);

		Assert.assertEquals(actual.getAuthorizationCode(), expected.getAuthorizationCode());
		Assert.assertEquals(actual.getTraceabilityId(), expected.getTraceabilityId());
		Assert.assertEquals(actual.getResponseData(), expected.getResponseData());
		Assert.assertEquals(actual.getMessage(), expected.getMessage());
		Assert.assertEquals(actual.getAcquirerResponseCode(), expected.getAcquirerResponseCode());
		Assert.assertEquals(actual.getResponseCode(), expected.getResponseCode());
	}

	/**
	 * Test that method {@linkplain ResponseFactory#createCaptureResponseMessage} successfully creates the capture response message.
	 */
	@Test
	public void createCaptureResponseMessage() {

		final CaptureRequestMessage captureRequestMessage = CaptureRequestMessage
				.withCorrelationId("123456")
				.withAmount(AmountDto.of("COP", new BigDecimal(200000)).build())
				.withAuthorizationTraceabilityIdentifier("11111").build();

		final NetworkResponse networkResponse = NetworkResponse.builder().amount("200000").orderId("11111")
				.responseCode("0")
				.transactionDate(new Date().toString()).responseMessage("Approved transaction").state("ACCEPTED")
				.transactionCode("CAPTURED").build();

		ObjectMapper oMapper = new ObjectMapper();
		final CaptureResponseMessage expected = CaptureResponseMessage.withCorrelationId("123456")
				.withCaptureCode("ACCEPTED")
				.withTraceabilityId("11111")
				.withMessage("Approved transaction")
				.withAcquirerResponseCode("CAPTURED")
				.withResponseCode(CaptureResponseCode.APPROVED)
				.withResponseData(oMapper.convertValue(networkResponse, Map.class))
				.build();
		CaptureResponseMessage actual = ResponseFactory
				.createCaptureResponseMessage(captureRequestMessage, networkResponse);
		Assert.assertEquals(actual.getCaptureCode(), expected.getCaptureCode());
		Assert.assertEquals(actual.getTraceabilityId(), expected.getTraceabilityId());
		Assert.assertEquals(actual.getResponseData(), expected.getResponseData());
		Assert.assertEquals(actual.getMessage(), expected.getMessage());
		Assert.assertEquals(actual.getAcquirerResponseCode(), expected.getAcquirerResponseCode());
		Assert.assertEquals(actual.getResponseCode(), expected.getResponseCode());

	}

	/**
	 * Test that method {@linkplain ResponseFactory#createVoidResponseMessage} successfully creates the void response message.
	 */
	@Test
	public void createVoidResponseMessage() {

		final VoidRequestMessage voidRequestMessage = VoidRequestMessage.withCorrelationId("12345")
				.withAuthorizationTraceabilityIdentifier(null)
				.build();

		final NetworkResponse networkResponse = NetworkResponse.builder().orderId("11111")
				.responseCode("0")
				.transactionDate(new Date().toString()).responseMessage("Approved transaction").state("ACCEPTED")
				.transactionCode("VOIDED").build();

		ObjectMapper oMapper = new ObjectMapper();
		final VoidResponseMessage expected = VoidResponseMessage.withCorrelationId("123456")
				.withVoidCode("ACCEPTED")
				.withTraceabilityId("11111")
				.withMessage("Approved transaction")
				.withAcquirerResponseCode("VOIDED")
				.withResponseCode(VoidResponseCode.APPROVED)
				.withResponseData(oMapper.convertValue(networkResponse, Map.class))
				.build();
		VoidResponseMessage actual = ResponseFactory
				.createVoidResponseMessage(voidRequestMessage, networkResponse);
		Assert.assertEquals(actual.getVoidCode(), expected.getVoidCode());
		Assert.assertEquals(actual.getTraceabilityId(), expected.getTraceabilityId());
		Assert.assertEquals(actual.getResponseData(), expected.getResponseData());
		Assert.assertEquals(actual.getMessage(), expected.getMessage());
		Assert.assertEquals(actual.getAcquirerResponseCode(), expected.getAcquirerResponseCode());
		Assert.assertEquals(actual.getResponseCode(), expected.getResponseCode());

	}

	/**
	 * Test that method {@linkplain ResponseFactory#createRefundResponseMessage} successfully creates the refund response message.
	 */
	@Test
	public void createRefundResponseMessage() {

		final RefundRequestMessage refundRequestMessage = RefundRequestMessage.withCorrelationId("12345")
				.withCaptureTraceabilityId(null)
				.build();

		final NetworkResponse networkResponse = NetworkResponse.builder().orderId("11111")
				.responseCode("0")
				.transactionDate(new Date().toString()).responseMessage("Approved transaction").state("ACCEPTED")
				.transactionCode("REFUNDED").build();

		ObjectMapper oMapper = new ObjectMapper();
		final RefundResponseMessage expected = RefundResponseMessage.withCorrelationId("123456")
				.withRefundCode("ACCEPTED")
				.withTraceabilityId("11111")
				.withMessage("Approved transaction")
				.withAcquirerResponseCode("REFUNDED")
				.withResponseCode(RefundResponseCode.APPROVED)
				.withResponseData(oMapper.convertValue(networkResponse, Map.class))
				.build();
		RefundResponseMessage actual = ResponseFactory
				.createRefundResponseMessage(refundRequestMessage, networkResponse);
		Assert.assertEquals(actual.getRefundCode(), expected.getRefundCode());
		Assert.assertEquals(actual.getTraceabilityId(), expected.getTraceabilityId());
		Assert.assertEquals(actual.getResponseData(), expected.getResponseData());
		Assert.assertEquals(actual.getMessage(), expected.getMessage());
		Assert.assertEquals(actual.getAcquirerResponseCode(), expected.getAcquirerResponseCode());
		Assert.assertEquals(actual.getResponseCode(), expected.getResponseCode());

	}
}