/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import org.apache.activemq.ActiveMQSslConnectionFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@linkplain BrokerConfigurator} class
 *
 * @author David Hidalgo (david.hidalgo@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class BrokerConfiguratorTest {

	/**
	 * JMS Configurator (Class under testing)
	 */
	private BrokerConfigurator configurator;

	/**
	 * Set up test environment before each test
	 */
	@BeforeMethod
	public void setUp() {

		configurator = new BrokerConfigurator();
		configurator.setBrokerUrl("vm://localhost");
		configurator.setKeyStore("/keystore.ks");
		configurator.setKeyStorePassword("keystorePassword");
		configurator.setTrustStore("/Truststore.ts");
		configurator.setTrustStorePassword("truststorePassword");

	}

	/**
	 * Test the creation of the JMS Component used to send and receive messages from the broker
	 *
	 * @author david.hidalgo
	 * @throws Exception
	 * @date 30/08/2016
	 */
	@Test
	public void testJmsComponentCreation() throws Exception {

		configurator.setKeyStore(null);
		configurator.setTrustStore(null);
		assertThat(configurator.buildSslConnectionFactory())
				.isNotNull()
				.satisfies(comp -> assertThat(comp).isNotInstanceOf(ActiveMQSslConnectionFactory.class));

	}

	/**
	 * Test the creation of the JMS Component used to send and receive messages from the broker over SSL
	 *
	 * @author david.hidalgo
	 * @throws Exception
	 * @date 30/08/2016
	 */
	@Test
	public void testJmsComponentSSLCreationTrustStoreIsNull() {

		configurator.setTrustStore(null);
		assertThat(configurator.buildSslConnectionFactory())
				.isNotNull()
				.satisfies(comp -> assertThat(comp).isInstanceOf(ActiveMQSslConnectionFactory.class));
	}
	
	/**
	 * Test the creation of the JMS Component used to send and receive messages from the broker over SSL
	 *
	 * @author david.hidalgo
	 * @date 30/08/2016
	 */
	@Test
	public void testJmsComponentSSLCreationKeystoreIsNull() {
		
		configurator.setTrustStore("/truststore");
		configurator.setKeyStore(null);
		assertThat(configurator.buildSslConnectionFactory())
				.isNotNull()
				.satisfies(comp -> assertThat(comp).isInstanceOf(ActiveMQSslConnectionFactory.class));
	}

	/**
	 * Test set and get methods
	 *
	 * @author manuel.vieda
	 * @date 30/08/2016
	 */
	@Test
	public void testSetAndGetProperties() {

		assertThat(configurator.getBrokerUrl()).isNotNull().isEqualTo("vm://localhost");
		assertThat(configurator.getKeyStore()).isNotNull().isEqualTo("/keystore.ks");
		assertThat(configurator.getKeyStorePassword()).isNotNull().isEqualTo("keystorePassword");
		assertThat(configurator.getTrustStore()).isNotNull().isEqualTo("/Truststore.ts");
		assertThat(configurator.getTrustStorePassword()).isNotNull().isEqualTo("truststorePassword");

	}

}
