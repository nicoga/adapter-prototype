/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application;

import com.payu.houston.adapter.queue.model.common.AmountDetail;
import com.payu.houston.adapter.queue.model.common.AmountDto;
import com.payu.houston.adapter.queue.model.common.RouteDto;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidResponseMessage;
import com.payu.houston.adapter.test.JmsIntegrationTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test suite for Void operation
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class VoidOperationIntegrationTest extends JmsIntegrationTest {

	/**
	 * Test a Void operation against the Test Adapter
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testSendAndReceiveVoid() {

		final VoidRequestMessage request = createSampleRequest();
		assertThat(request).isNotNull();

		final Instant start = Instant.now();
		sendMessage(request);
		final Instant stop = Instant.now();

		final VoidResponseMessage responseMessage = receiveVoidResponseMessage();
		assertThat(responseMessage)
				.as("The response message for the adapter must generate a complete response")
				.isNotNull()
				.hasFieldOrProperty("correlationId")
				.hasFieldOrProperty("traceabilityId")
				.hasFieldOrProperty("responseCode")
				.hasFieldOrProperty("acquirerResponseCode")
				.hasFieldOrProperty("message");

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isLessThan(Duration.of(1, ChronoUnit.SECONDS));
	}

	/**
	 * Creates a sample Void Request message
	 *
	 * @return A new Void Request sample message
	 */
	public VoidRequestMessage createSampleRequest() {

		return VoidRequestMessage
				.withCorrelationId(UUID.randomUUID().toString())
				.withAmount(AmountDto
						.of("USD", BigDecimal.valueOf(10.50))
						.withDetail(AmountDetail.of(BigDecimal.valueOf(1.50)).build())
						.build())
				.withRoute(RouteDto
						.withId(UUID.randomUUID().toString())
						.addExtraParameter("Key1", "Value1")
						.build())
				.addPaymentInstrument("PAYMENT_TYPE", "CREDIT_CARD")
				.addPaymentInstrument("PAYMENT_BRAND", "VISA")
				.addPaymentInstrument("CARD_PAN", "4242424242424242")
				.addPaymentInstrument("CARD_EXPIRATION_DATE", "2025/12")
				.addPaymentInstrument("CARD_SECURITY_CODE", "000")
				.addPaymentInstrument("INSTALLMENTS", "1")
				.build();
	}

}
