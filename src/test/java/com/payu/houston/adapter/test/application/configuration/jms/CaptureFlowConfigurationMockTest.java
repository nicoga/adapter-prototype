/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test case for {@link CaptureFlowConfiguration} class
 *
 * @author Santiago Alzate S. (santiago.alzate@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class CaptureFlowConfigurationMockTest extends GenericFlowMockTest {

	/**
	 * Capture flow/route configuration component (Class under testing)
	 */
	private final CaptureFlowConfiguration captureFlowConfiguration = new CaptureFlowConfiguration();

	/**
	 * Set up test environment
	 */
	@BeforeClass
	public void setUp() {

		super.setUp(captureFlowConfiguration);
	}

	/**
	 * Test case for {@linkplain CaptureFlowConfiguration#getProviderHandlerMethod()} method
	 *
	 * @author Santiago Alzate S. (santiago.alzate@payulatam.com)
	 * @date 27/07/2016
	 */
	@Test
	public void testGetProviderHandlerMethod() {

		assertThat(
				captureFlowConfiguration.getProviderHandlerMethod()).isNotEmpty();
	}

}