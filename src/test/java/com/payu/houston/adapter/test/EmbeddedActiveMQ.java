/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Embedded ActiveMQ Broker implementation
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class EmbeddedActiveMQ {

	/**
	 * Class logger
	 */
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * ActiveMQ Broker servcice
	 */
	private final BrokerService brokerService;

	/**
	 * Create an embedded ActiveMQ broker using defaults values:
	 * <p>
	 * <ul>
	 * <li>the broker name is 'embedded-broker'</li>
	 * <li>JMX is disabled</li>
	 * <li>Persistence is disabled</li>
	 * </ul>
	 */
	public EmbeddedActiveMQ() {

		brokerService = new BrokerService();
		brokerService.setUseJmx(false);
		brokerService.setUseShutdownHook(false);
		brokerService.setPersistent(false);
		brokerService.setBrokerName("embedded-broker");
	}

	/**
	 * Customize the configuration of the embedded ActiveMQ broker
	 * <p>
	 * This method is called before the embedded ActiveMQ broker is started, and can be overridden to this method to
	 * customize the broker configuration.
	 */
	protected void configure() {

	}

	/**
	 * Start the embedded ActiveMQ broker, blocking until the broker has successfully started.
	 * <p/>
	 * The broker will normally be started by Test Framework using the before() method. This method allows the broker to
	 * be started manually to support advanced testing scenarios.
	 */
	public void start() {

		try {
			configure();
			brokerService.start();
		} catch (final Exception ex) {
			throw new RuntimeException(
					"Exception encountered starting embedded ActiveMQ broker: " + getBrokerName(), ex);
		}

		brokerService.waitUntilStarted();
	}

	/**
	 * Stop the embedded ActiveMQ broker, blocking until the broker has stopped.
	 * <p/>
	 * The broker will normally be stopped by Test Framework using the after() method. This method allows the broker to
	 * be stopped manually to support advanced testing scenarios.
	 */
	public void stop() {

		if (!brokerService.isStopped()) {
			try {
				brokerService.stop();
			} catch (final Exception ex) {
				log.warn("Exception encountered stopping embedded ActiveMQ broker: " + getBrokerName(), ex);
			}
		}

		brokerService.waitUntilStopped();
	}

	/**
	 * Create an ActiveMQConnectionFactory for the embedded ActiveMQ Broker
	 *
	 * @return a new ActiveMQConnectionFactory
	 */
	public ActiveMQConnectionFactory createConnectionFactory() {

		final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL(brokerService.getVmConnectorURI().toString());
		return connectionFactory;
	}

	/**
	 * Get the BrokerService for the embedded ActiveMQ broker.
	 * <p/>
	 * This may be required for advanced configuration of the BrokerService.
	 *
	 * @return the embedded ActiveMQ broker
	 */
	public BrokerService getBrokerService() {

		return brokerService;
	}

	/**
	 * Get the VM URL for the embedded ActiveMQ Broker
	 * <p/>
	 * NOTE: The option is precreate=false option is appended to the URL to avoid the automatic creation of brokers
	 * and the resulting duplicate broker errors
	 *
	 * @return the VM URL for the embedded broker
	 */
	public String getVmURL() {

		return String.format("failover:(%s?create=false)", brokerService.getVmConnectorURI().toString());
	}

	/**
	 * Get the name of the embedded ActiveMQ Broker
	 *
	 * @return name of the embedded broker
	 */
	public String getBrokerName() {

		return brokerService.getBrokerName();
	}

	/**
	 * Sest the broker name
	 *
	 * @param brokerName The brober name to set
	 */
	public void setBrokerName(final String brokerName) {

		brokerService.setBrokerName(brokerName);
	}
}
