/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

/**
 * Test utility to validate private constructor for utility classes.
 *
 * @author Manuel E. Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public final class AccessModifierTestUtil {

	/**
	 * Default empty private constructor for utility class
	 */
	private AccessModifierTestUtil() {

		// Default empty constructor
	}

	/**
	 * Validates the constructor of the utility class.
	 * <p>
	 * An utility class must have a single private constructor.
	 *
	 * @param clasz The class to validate
	 * @author Manuel E. Vieda (manuel.vieda@payulatam.com)
	 * @date 11/08/2015
	 */
	public static void testUtilityClassConstructor(final Class<?> clasz) {

		@SuppressWarnings("rawtypes")
		final Constructor[] constructors = clasz.getDeclaredConstructors();
		assertThat(constructors)
				.as(clasz + " Utility class should only have one constructor")
				.hasSize(1);

		@SuppressWarnings("rawtypes")
		final Constructor constructor = constructors[0];
		assertThat(constructor.isAccessible())
				.as(clasz + " Utility class constructor should be inaccessible")
				.isFalse();

		assertThat(constructor.getModifiers())
				.as(clasz + " Utility class constructor modifier should be private")
				.isNotNull().isEqualTo(Modifier.PRIVATE);

		assertThatClassCannotBeInstantiated(clasz);

		try {
			constructor.setAccessible(true);
			constructor.newInstance((Object[]) null);
		} catch (final InstantiationException | IllegalAccessException | IllegalArgumentException |
				InvocationTargetException e) {
			fail(e.getMessage(), e);
		}

	}

	/**
	 * Test that the given class cannot be instantiated directly.
	 *
	 * @param clasz The class to validate
	 */
	private static void assertThatClassCannotBeInstantiated(final Class<?> clasz) {

		try {
			clasz.newInstance();
			fail(clasz + " Utility class should be restricted for instantiation");

		} catch (final IllegalAccessException e) {
			// Expected exception
		} catch (final InstantiationException e) {
			fail(e.getMessage(), e);
		}
	}

}
