/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import com.payu.houston.adapter.test.JmsIntegrationTest;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSslConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@linkplain JmsConfigurator} class
 *
 * @author Santiago Alzate S. (santiago.alzate@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class BrokerConfiguratorIntegrationTest extends JmsIntegrationTest {

	/**
	 * JMS Configurator (Class under testing)
	 */
	@Autowired
	private BrokerConfigurator configurator;

	/**
	 * Test the creation of the JMS Component used to send and receive messages from the broker
	 *
	 * @author manuel.vieda
	 * @date 30/08/2016
	 */
	@Test
	public void testBuildConnectionFactory() {

		final ActiveMQConnectionFactory connectionFactory = configurator.buildSslConnectionFactory();
		assertThat(connectionFactory)
				.isNotNull()
				.isInstanceOf(ActiveMQConnectionFactory.class)
				.isNotInstanceOf(ActiveMQSslConnectionFactory.class);
	}

	/**
	 * Test properties are not null (Empty if not defined)
	 *
	 * @author david.hidalgo
	 * @date 30/08/2016
	 */
	@Test
	public void testNonNullParameters() {

		assertThat(configurator.getBrokerUrl()).isNotNull();
		assertThat(configurator.getKeyStore()).isNotNull().isEmpty();
		assertThat(configurator.getTrustStore()).isNotNull().isEmpty();
		assertThat(configurator.getKeyStorePassword()).isNotNull().isEmpty();
		assertThat(configurator.getTrustStorePassword()).isNotNull().isEmpty();
	}
}