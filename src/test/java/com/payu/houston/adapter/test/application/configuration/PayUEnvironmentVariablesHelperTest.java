/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Test suite for {@link PayUEnvironmentVariablesHelperTest} class
 *
 * @author Manuel E. Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class PayUEnvironmentVariablesHelperTest {

	/**
	 * Initialize variables (Read environment variables) and verify its values.
	 *
	 * @author Manuel E. Vieda (manuel.vieda@payulatam.com)
	 * @date 11/08/2015
	 */
	@Test
	public void initAndReturnEnvVars() {

		final PayUEnvironmentVariablesHelper helper = new PayUEnvironmentVariablesHelper(null, null, null);
		Assert.assertNotNull(helper.getPayuIter());
		Assert.assertNotNull(helper.getPayuSalt());
		Assert.assertNotNull(helper.getPayuKey());

		Assert.assertEquals(helper.getPayuIter(),
				System.getenv(PayUEnvironmentVariablesHelper.PAYU_ITER));
		Assert.assertEquals(helper.getPayuSalt(),
				System.getenv(PayUEnvironmentVariablesHelper.PAYU_SAL));
		Assert.assertEquals(helper.getPayuKey(),
				System.getenv(PayUEnvironmentVariablesHelper.PAYU_KEY));

	}
}