/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.provider;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseCode;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseMessage;
import com.payu.houston.adapter.queue.model.common.AmountDto;
import com.payu.houston.adapter.queue.model.common.OrderDto;
import com.payu.houston.adapter.test.model.NetworkResponse;
import com.payu.houston.adapter.test.model.constant.ParametersName;
import com.payu.houston.adapter.test.service.impl.NetworkServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * Test cases for the {@link HoustonTestProvider} class
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ RequestValidator.class, ResponseFactory.class })
public class HoustonTestProviderTest {

	/**
	 * Instance of tested class.
	 */
	@InjectMocks
	private HoustonTestProvider houstonTestProvider;

	/**
	 * Service used in {@linkplain HoustonTestProvider}.
	 */
	@Mock
	private NetworkServiceImpl networkService;

	/**
	 * Authorization request message used in test cases.
	 */
	private AuthorizationRequestMessage request;

	/**
	 * Mocked network response used in test cases.
	 */
	private NetworkResponse networkResponse;

	@Before
	public void before() {

		request = AuthorizationRequestMessage
				.withCorrelationId("123456").withOrder(
						OrderDto.withId(1234455).build())
				.withAmount(AmountDto.of("COP", new BigDecimal(200000)).build())
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567890123")
				.addPaymentInstrument(ParametersName.PAYER_IDENTITY_DOC_NUMBER.name(), "12345678910")
				.addPaymentInstrument(ParametersName.PAYER_IDENTITY_DOC_TYPE.name(), "CC")
				.addPaymentInstrument(ParametersName.PAYER_EMAIL.name(), "email@gmail.com")
				.addPaymentInstrument(ParametersName.CARDHOLDER_NAME.name(), "Nicolas Garcia")
				.addPaymentInstrument(ParametersName.PAYER_PHONE.name(), "9876543210")
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567894367")
				.addPaymentInstrument(ParametersName.CARD_SECURITY_CODE.name(), "123")
				.addPaymentInstrument(ParametersName.CARD_EXPIRATION_DATE.name(), "12/2023")
				.addPaymentInstrument(ParametersName.INSTALLMENTS.name(), "12")
				.build();
		networkResponse = NetworkResponse.builder().amount("200000").orderId("11111")
				.responseCode("0")
				.transactionDate(new Date().toString()).responseMessage("Approved transaction").state("ACCEPTED")
				.transactionCode("AUTHORIZED").build();

	}

	/**
	 * Test that {@linkplain HoustonTestProvider#doAuthorization} method successfully do an authorization.
	 */
	@Test
	public void shouldDoAuthorization() {

		AuthorizationResponseMessage authorizationResponseMessage = AuthorizationResponseMessage
				.withCorrelationId("123456")
				.withAuthorizationCode("ACCEPTED")
				.withTraceabilityId("11111")
				.withMessage("Approved transaction")
				.withAcquirerResponseCode("AUTHORIZED")
				.withResponseCode(AuthorizationResponseCode.APPROVED)
				.build();

		mockStatic(RequestValidator.class);
		when(RequestValidator.validateAuthorizationRequestMessage(any(AuthorizationRequestMessage.class)))
				.thenReturn(true);
		when(networkService.doAuthorization(any(AuthorizationRequestMessage.class))).thenReturn(networkResponse);
		mockStatic(ResponseFactory.class);
		when(ResponseFactory.createAuthorizationResponseMessage(any(AuthorizationRequestMessage.class),
				any(NetworkResponse.class))).thenReturn(authorizationResponseMessage);
		Assert.assertEquals(authorizationResponseMessage, houstonTestProvider.doAuthorization(request));
		verify(networkService).doAuthorization(any(AuthorizationRequestMessage.class));
		verifyStatic(RequestValidator.class);
		RequestValidator.validateAuthorizationRequestMessage(any(AuthorizationRequestMessage.class));
		verifyStatic(ResponseFactory.class);
		ResponseFactory
				.createAuthorizationResponseMessage(any(AuthorizationRequestMessage.class), any(NetworkResponse.class));
	}

	/**
	 * Test that {@linkplain HoustonTestProvider#doAuthorization} method does not process an authorization when an invalid request is present.
	 */
	@Test
	public void shouldNotDoAuthorizationWhenInvalidRequest() {

		AuthorizationResponseMessage authorizationResponseMessage = AuthorizationResponseMessage
				.withCorrelationId(request.getCorrelationId())
				.withResponseCode(AuthorizationResponseCode.ERROR)
				.withErrorMessage("Payment instrument, order and amount values must be present and valid.")
				.build();

		mockStatic(RequestValidator.class);
		when(RequestValidator.validateAuthorizationRequestMessage(any(AuthorizationRequestMessage.class)))
				.thenReturn(false);
		when(networkService.doAuthorization(any(AuthorizationRequestMessage.class))).thenReturn(networkResponse);
		mockStatic(ResponseFactory.class);
		when(ResponseFactory.createAuthorizationResponseMessage(any(AuthorizationRequestMessage.class),
				any(NetworkResponse.class))).thenReturn(authorizationResponseMessage);
		Assert.assertEquals(authorizationResponseMessage.getErrorMessage(),
				houstonTestProvider.doAuthorization(request).getErrorMessage());
		verifyStatic(RequestValidator.class);
		RequestValidator.validateAuthorizationRequestMessage(any(AuthorizationRequestMessage.class));
	}
}