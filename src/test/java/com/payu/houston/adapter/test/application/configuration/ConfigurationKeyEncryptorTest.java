/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test suite for {@linkplain ConfigurationKeyEncryptor} class
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class ConfigurationKeyEncryptorTest {

	/**
	 * Test ConfigurationKeyEncryptor creation and configuration
	 *
	 * @author Manuel E. Vieda (manuel.vieda@payulatam.com)
	 * @date 13/09/2016
	 */
	@Test
	public void testStandardPBEStringEncryptor() {

		final ConfigurationKeyEncryptor configurationKeyEncryptor = new ConfigurationKeyEncryptor();
		assertThat(configurationKeyEncryptor.standardPBEStringEncryptor(null, null, null))
				.isNotNull()
				.satisfies(encryptor -> {
					final String originalMessage = "Original-Message-Test";
					final String encMessage = encryptor.encrypt(originalMessage);
					assertThat(encMessage).isNotEmpty();
					assertThat(encryptor.decrypt(encMessage)).isEqualTo(originalMessage);
					assertThat(encryptor.isInitialized()).isTrue();
				});

	}

}