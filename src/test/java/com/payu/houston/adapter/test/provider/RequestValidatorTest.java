/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.provider;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.common.AmountDto;
import com.payu.houston.adapter.queue.model.common.OrderDto;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.test.model.constant.ParametersName;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Test cases for the {@link RequestValidator} class
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class RequestValidatorTest {

	/**
	 * Test that method {@linkplain RequestValidator#validateAuthorizationRequestMessage} successfully validates request.
	 */
	@Test
	public void validateAuthorizationRequestMessageSuccessfully() {

		AuthorizationRequestMessage request = AuthorizationRequestMessage
				.withCorrelationId("123456").withOrder(
						OrderDto.withId(1234455).build())
				.withAmount(AmountDto.of("COP", new BigDecimal(200000)).build())
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567890123")
				.addPaymentInstrument(ParametersName.PAYER_IDENTITY_DOC_NUMBER.name(), "12345678910")
				.addPaymentInstrument(ParametersName.PAYER_IDENTITY_DOC_TYPE.name(), "CC")
				.addPaymentInstrument(ParametersName.PAYER_EMAIL.name(), "email@gmail.com")
				.addPaymentInstrument(ParametersName.CARDHOLDER_NAME.name(), "Nicolas Garcia")
				.addPaymentInstrument(ParametersName.PAYER_PHONE.name(), "9876543210")
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567894367")
				.addPaymentInstrument(ParametersName.CARD_SECURITY_CODE.name(), "123")
				.addPaymentInstrument(ParametersName.CARD_EXPIRATION_DATE.name(), "12/2023")
				.addPaymentInstrument(ParametersName.INSTALLMENTS.name(), "12")
				.build();
		Assert.assertTrue(RequestValidator.validateAuthorizationRequestMessage(request));
	}

	/**
	 * Test that method {@linkplain RequestValidator#validateAuthorizationRequestMessage} does not validate a request with missing or wrong parameters.
	 */
	@Test
	public void shouldNotValidateWrongAuthorizationRequest() {

		AuthorizationRequestMessage request = AuthorizationRequestMessage
				.withCorrelationId("123456").withOrder(
						OrderDto.withId(1234455).build())
				.withAmount(AmountDto.of("COP", new BigDecimal(200000)).build())
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567890123")
				.addPaymentInstrument(ParametersName.PAYER_IDENTITY_DOC_NUMBER.name(), "12345678910")
				.addPaymentInstrument(ParametersName.PAYER_IDENTITY_DOC_TYPE.name(), "CC")
				.addPaymentInstrument(ParametersName.PAYER_EMAIL.name(), "email@gmail.com")
				.addPaymentInstrument(ParametersName.CARDHOLDER_NAME.name(), "Nicolas Garcia")
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567894367")
				.addPaymentInstrument(ParametersName.CARD_SECURITY_CODE.name(), "123")
				.addPaymentInstrument(ParametersName.CARD_EXPIRATION_DATE.name(), "12/2023")
				.addPaymentInstrument(ParametersName.INSTALLMENTS.name(), "12")
				.build();
		Assert.assertFalse(RequestValidator.validateAuthorizationRequestMessage(request));
	}

	/**
	 * Test that method {@linkplain RequestValidator#validateCaptureRequestMessage} successfully validates a capture request.
	 */
	@Test
	public void shouldValidateCaptureRequestSuccessfully() {

		CaptureRequestMessage captureRequestMessage = CaptureRequestMessage.withCorrelationId("12345").withAmount(
				AmountDto.of("COP", new BigDecimal(20000)).build()).withAuthorizationTraceabilityIdentifier("1111")
				.build();

		Assert.assertTrue(RequestValidator.validateCaptureRequestMessage(captureRequestMessage));
	}

	/**
	 * Test that method {@linkplain RequestValidator#validateCaptureRequestMessage} does not validate a capture request with missing or wrong parameters.
	 */
	@Test
	public void shouldNotValidateWrongCaptureRequest() {

		CaptureRequestMessage captureRequestMessage = CaptureRequestMessage.withCorrelationId("12345").withAmount(
				AmountDto.of("COP", new BigDecimal(20000)).build()).withAuthorizationTraceabilityIdentifier(null)
				.build();

		Assert.assertFalse(RequestValidator.validateCaptureRequestMessage(captureRequestMessage));
	}

	/**
	 * Test that method {@linkplain RequestValidator#validateVoidRequestMessage} successfully validates a void request.
	 */
	@Test
	public void shouldValidateVoidRequestSuccessfully() {

		VoidRequestMessage voidRequestMessage = VoidRequestMessage.withCorrelationId("12345")
				.withAuthorizationTraceabilityIdentifier("1111")
				.build();

		Assert.assertTrue(RequestValidator.validateVoidRequestMessage(voidRequestMessage));
	}

	/**
	 * Test that method {@linkplain RequestValidator#validateVoidRequestMessage} does not validate a void request with missing or wrong parameters.
	 */
	@Test
	public void shouldNotValidateWrongVoidRequest() {

		VoidRequestMessage voidRequestMessage = VoidRequestMessage.withCorrelationId("12345")
				.withAuthorizationTraceabilityIdentifier(null)
				.build();

		Assert.assertFalse(RequestValidator.validateVoidRequestMessage(voidRequestMessage));
	}

	/**
	 * Test that method {@linkplain RequestValidator#validateRefundRequest} successfully validates a refund request.
	 */
	@Test
	public void shouldValidateRefundRequestSuccessfully() {

		RefundRequestMessage refundRequestMessage = RefundRequestMessage.withCorrelationId("12345")
				.withCaptureTraceabilityId("1111").withAmount(AmountDto.of("COP", new BigDecimal(20000)).build())
				.build();

		Assert.assertTrue(RequestValidator.validateRefundRequest(refundRequestMessage));
	}

	/**
	 * Test that method {@linkplain RequestValidator#validateRefundRequest} does not validate a refund request with missing or wrong parameters.
	 */
	@Test
	public void shouldNotValidateWrongRefundRequest() {

		RefundRequestMessage refundRequestMessage = RefundRequestMessage.withCorrelationId("12345")
				.withCaptureTraceabilityId("1111")
				.build();

		Assert.assertFalse(RequestValidator.validateRefundRequest(refundRequestMessage));
	}
}