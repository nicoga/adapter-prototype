/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import com.payu.houston.adapter.test.JmsIntegrationTest;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@linkplain JmsConfigurator} class
 *
 * @author Santiago Alzate S. (santiago.alzate@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class JmsConfiguratorIntegrationTest extends JmsIntegrationTest {

	/**
	 * JMS Configurator (Class under testing)
	 */
	@Autowired
	private JmsConfigurator configurator;

	/**
	 * Test the creation of the JMS Connection Factory used to send and receive messages from the broker
	 *
	 * @author manuel.vieda
	 * @throws Exception 
	 * @date 30/08/2016
	 */
	@Test
	public void testJmsConnectionFactoryCreation() throws Exception {

		final PooledConnectionFactory fact = configurator.getConnectionFactory(new BrokerConfigurator());
		assertThat(fact)
				.isNotNull()
				.satisfies(comp -> {
					assertThat(comp.getConnectionFactory()).isNotNull().isInstanceOf(ActiveMQConnectionFactory.class);
					assertThat(((ActiveMQConnectionFactory)comp.getConnectionFactory()).getBrokerURL()).isNotNull();
				});

	}

	/**
	 * Test the creation of the JMS Component used to send and receive messages from the broker
	 *
	 * @author manuel.vieda
	 * @throws Exception 
	 * @date 30/08/2016
	 */
	@Test
	public void testJmsComponentCreation() throws Exception {

		final BrokerConfigurator broker = new BrokerConfigurator();
		broker.setBrokerUrl("vm://localhost");
		final JmsComponent jmsComponent = configurator.getJmsComponent(broker.buildSslConnectionFactory());
		assertThat(jmsComponent)
				.isNotNull()
				.satisfies(comp -> {
					assertThat(comp.getConfiguration()).isNotNull();
					assertThat(comp.getCamelContext()).isNotNull();
					assertThat(comp.getStatus()).isNotNull();
				});

	}

	/**
	 * Test properties are not null
	 *
	 * @author manuel.vieda
	 * @date 30/08/2016
	 */
	@Test
	public void testNonNullParameters() {

		assertThat(configurator.isPreserveMessageQos()).isNotNull().isFalse();
		assertThat(configurator.getConcurrentConsumers()).isNotNull().isEqualTo(1);
		assertThat(configurator.getMaxConcurrentConsumers()).isNotNull().isEqualTo(5);
		assertThat(configurator.getMaxMessagesPerTask()).isNotNull().isEqualTo(100);
		assertThat(configurator.isAsyncConsumer()).isNotNull().isFalse();
		assertThat(configurator.isDisableReplyTo()).isNotNull().isFalse();
		assertThat(configurator.isDeliveryPersistent()).isNotNull().isTrue();
		assertThat(configurator.getTimeToLive()).isNotNull().isEqualTo(5000);
		assertThat(configurator.isCopyMessageOnSend()).isNotNull().isFalse();
		assertThat(configurator.isUseCompression()).isNotNull().isTrue();
		assertThat(configurator.getMaxThreadPoolSize()).isNotNull().isEqualTo(300);
		assertThat(configurator.isMessagePrioritySupported()).isNotNull().isFalse();
		assertThat(configurator.isDispatchAsync()).isNotNull().isFalse();
		assertThat(configurator.isUseAsyncSend()).isNotNull().isFalse();
		assertThat(configurator.isStatsEnabled()).isNotNull().isFalse();
	}
}