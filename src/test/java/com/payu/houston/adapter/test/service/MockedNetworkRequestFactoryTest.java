/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.service;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.common.AmountDto;
import com.payu.houston.adapter.queue.model.common.OrderDto;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.test.model.CardHolder;
import com.payu.houston.adapter.test.model.CreditCard;
import com.payu.houston.adapter.test.model.Merchant;
import com.payu.houston.adapter.test.model.Order;
import com.payu.houston.adapter.test.model.constant.ParametersName;
import com.payu.houston.adapter.test.model.constant.TransactionTypes;
import com.payu.houston.adapter.test.model.request.NetworkAuthorizationRequest;
import com.payu.houston.adapter.test.model.request.NetworkCaptureRequest;
import com.payu.houston.adapter.test.model.request.NetworkRefundRequest;
import com.payu.houston.adapter.test.model.request.NetworkVoidRequest;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Test cases for the {@link MockedNetworkRequestFactory} class.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class MockedNetworkRequestFactoryTest {

	/**
	 * Test that {@linkplain MockedNetworkRequestFactory#createAuthorizationRequest} method successfully creates an authorization request for the mocked network.
	 */
	@Test
	public void createAuthorizationRequest() {

		AuthorizationRequestMessage authorizationRequestMessage = AuthorizationRequestMessage
				.withCorrelationId("123456").withOrder(
						OrderDto.withId(1234455).build())
				.withAmount(AmountDto.of("COP", new BigDecimal(200000)).build())
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567890123")
				.addPaymentInstrument(ParametersName.PAYER_IDENTITY_DOC_NUMBER.name(), "12345678910")
				.addPaymentInstrument(ParametersName.PAYER_IDENTITY_DOC_TYPE.name(), "CC")
				.addPaymentInstrument(ParametersName.PAYER_EMAIL.name(), "email@gmail.com")
				.addPaymentInstrument(ParametersName.CARDHOLDER_NAME.name(), "Nicolas Garcia")
				.addPaymentInstrument(ParametersName.PAYER_PHONE.name(), "9876543210")
				.addPaymentInstrument(ParametersName.CARD_SECURITY_CODE.name(), "123")
				.addPaymentInstrument(ParametersName.CARD_EXPIRATION_DATE.name(), "12/2023")
				.addPaymentInstrument(ParametersName.INSTALLMENTS.name(), "12")
				.build();

		NetworkAuthorizationRequest expectedResult = NetworkAuthorizationRequest.builder()
				.cardHolder(
						CardHolder.builder().documentNumber("12345678910").documentType("CC").email("email@gmail.com")
								.fullName("Nicolas Garcia").phoneNumber("9876543210").build())
				.creditCard(CreditCard.builder().cardNumber("1234567890123").cvv("123").expirationMonth("12")
						.expirationYear("2023").build())
				.merchant(Merchant.builder().build())
				.order(Order.builder().currency("COP").amount("200000.00").installments(12).orderId("1234455").build())
				.type(TransactionTypes.AUTHORIZATION.name()).build();

		Assert.assertEquals(expectedResult,
				MockedNetworkRequestFactory.createAuthorizationRequest(authorizationRequestMessage));

	}

	/**
	 * Test that {@linkplain MockedNetworkRequestFactory#createCaptureRequest} method successfully creates a capture request for the mocked network.
	 */
	@Test
	public void createCaptureRequestTest() {

		CaptureRequestMessage captureRequestMessage = CaptureRequestMessage.withCorrelationId("12345").withAmount(
				AmountDto.of("COP", new BigDecimal(20000)).build()).withAuthorizationTraceabilityIdentifier("1111")
				.build();

		NetworkCaptureRequest expectedResult = NetworkCaptureRequest.builder().captureAmount("20000.00")
				.orderId("1111").merchant(Merchant.builder().apiKey(null).apiLogin(null).build()).build();

		Assert.assertEquals(expectedResult, MockedNetworkRequestFactory.createCaptureRequest(captureRequestMessage));
	}

	/**
	 * Test that {@linkplain MockedNetworkRequestFactory#createVoidRequest} method successfully creates a void request for the mocked network.
	 */
	@Test
	public void createVoidRequestTest() {

		VoidRequestMessage voidRequestMessage = VoidRequestMessage.withCorrelationId("12345")
				.withAuthorizationTraceabilityIdentifier("1111")
				.build();
		NetworkVoidRequest expectedResult = NetworkVoidRequest.builder()
				.orderId("1111").merchant(Merchant.builder().apiKey(null).apiLogin(null).build()).build();
		Assert.assertEquals(expectedResult, MockedNetworkRequestFactory.createVoidRequest(voidRequestMessage));
	}

	/**
	 * Test that {@linkplain MockedNetworkRequestFactory#createRefundRequest} method successfully creates a refund request for the mocked network.
	 */
	@Test
	public void createRefundRequestTest() {

		RefundRequestMessage voidRequestMessage = RefundRequestMessage.withCorrelationId("12345")
				.withCaptureTraceabilityId("1111").withAmount(AmountDto.of("COP", new BigDecimal(20000)).build())
				.build();
		NetworkRefundRequest expectedResult = NetworkRefundRequest.builder()
				.orderId("1111").merchant(Merchant.builder().apiKey(null).apiLogin(null).build())
				.refundAmount("20000.00").reason("Merchant requested").build();
		Assert.assertEquals(expectedResult, MockedNetworkRequestFactory.createRefundRequest(voidRequestMessage));
	}
}