/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseCode;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseMessage;
import com.payu.houston.adapter.queue.model.common.*;
import com.payu.houston.adapter.test.JmsIntegrationTest;
import org.springframework.test.annotation.Timed;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test suite for Authorization operation
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class AuthorizationOperationIntegrationTest extends JmsIntegrationTest {

	/**
	 * Test an Authorization operation against the Test Adapter
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	@Timed(millis = 5000)
	public void testSendAndReceiveAuthorization() {

		final AuthorizationRequestMessage authorizationRequest = createSampleAuthorizationRequest().build();
		assertThat(authorizationRequest).isNotNull();

		log.trace("testSendAndReceiveAuthorization:  Authorization Request: {}", authorizationRequest);

		final Instant start = Instant.now();
		sendMessage(authorizationRequest);
		final AuthorizationResponseMessage responseMessage = receiveAuthorizationResponse();
		final Instant stop = Instant.now();

		log.trace("testSendAndReceiveAuthorization:  Authorization Response: {} ", responseMessage);

		assertThat(responseMessage)
				.as("The response message for the adapter must generate a complete response")
				.isNotNull()
				.hasFieldOrProperty("correlationId")
				.hasFieldOrProperty("traceabilityId")
				.hasFieldOrProperty("responseCode")
				.hasFieldOrProperty("acquirerResponseCode")
				.hasFieldOrProperty("message");

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isLessThan(Duration.of(1, ChronoUnit.SECONDS));
	}

	/**
	 * Test that the acquirer must response with an APPROVED response code when the name of the payer is APPROVED
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testApprovedAuthorizationRequest() {

		testResponseCodeFromPayerName("APPROVED", AuthorizationResponseCode.APPROVED);
	}

	/**
	 * Test that the acquirer must response with an DECLINED response code when the name of the payer is
	 * ENTITY_DECLINED
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testDeclinedAuthorizationRequest() {

		testResponseCodeFromPayerName("ENTITY_DECLINED", AuthorizationResponseCode.ENTITY_DECLINED);
	}

	/**
	 * Test that the acquirer must response with an PAYMENT_NETWORK_REJECTED response code when the name of the payer is
	 * NETWORK_DECLINED
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testPaymentNetworkRejectedAuthorizationRequest() {

		testResponseCodeFromPayerName("NETWORK_DECLINED", AuthorizationResponseCode.PAYMENT_NETWORK_REJECTED);
	}

	/**
	 * Test that the acquirer must response with an PAYMENT_NETWORK_BAD_RESPONSE response code when the name of the
	 * payer is NETWORK_BAD_RESPONSE
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testPaymentNetworkBadResponseAuthorizationRequest() {

		testResponseCodeFromPayerName("NETWORK_BAD_RESPONSE", AuthorizationResponseCode.PAYMENT_NETWORK_BAD_RESPONSE);
	}

	/**
	 * Test that the acquirer must response with an PAYMENT_NETWORK_NO_RESPONSE response code when the name of the payer
	 * is NETWORK_NO_RESPONSE
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testPaymentNetworkNoResponseAuthorizationRequest() {

		testResponseCodeFromPayerName("NETWORK_NO_RESPONSE", AuthorizationResponseCode.PAYMENT_NETWORK_NO_RESPONSE);
	}

	/**
	 * Test that the acquirer must response with an ERROR response code when the name of the payer is ERROR
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testErrorResponseAuthorizationRequest() {

		testResponseCodeFromPayerName("ERROR", AuthorizationResponseCode.ERROR);
	}

	/**
	 * Test an Authorization operation against the Test Adapter
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testSendAndReceiveAuthorizationWithDelay() {

		final AuthorizationRequestMessage authorizationRequest = createSampleAuthorizationRequest()
				.addPaymentInstrument("CARD_SECURITY_CODE", "502")
				.build();
		assertThat(authorizationRequest).isNotNull();

		log.trace("testSendAndReceiveAuthorization:  Authorization Request: {}", authorizationRequest);

		final Instant start = Instant.now();
		sendMessage(authorizationRequest);
		final AuthorizationResponseMessage responseMessage = receiveAuthorizationResponse();
		final Instant stop = Instant.now();

		log.trace("testSendAndReceiveAuthorization:  Authorization Response: {} ", responseMessage);

		assertThat(responseMessage)
				.as("The response message for the adapter must generate a complete response")
				.isNotNull()
				.hasFieldOrProperty("correlationId")
				.hasFieldOrProperty("traceabilityId")
				.hasFieldOrProperty("responseCode")
				.hasFieldOrProperty("acquirerResponseCode")
				.hasFieldOrProperty("message");

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isGreaterThanOrEqualTo(Duration.of(2, ChronoUnit.SECONDS))
				.isLessThan(Duration.of(3, ChronoUnit.SECONDS));
	}

	/**
	 * Send and authorization request with the given payer name. Validates that the response from the adapter must have
	 * a specific response code.
	 *
	 * @param payerName The payer name to set to the request
	 * @param expectedAuthorizationResponseCode The expected response code
	 */
	private void testResponseCodeFromPayerName(final String payerName, final AuthorizationResponseCode
			expectedAuthorizationResponseCode) {

		final AuthorizationRequestMessage authorizationRequest = createSampleAuthorizationRequest()
				.addPaymentInstrument("PAYER_FULL_NAME", payerName)
				.build();
		assertThat(authorizationRequest).isNotNull();

		sendMessage(authorizationRequest);

		final AuthorizationResponseMessage responseMessage = receiveAuthorizationResponse();
		assertThat(responseMessage)
				.as("The response message for the adapter must generate a complete response")
				.isNotNull()
				.hasFieldOrProperty("correlationId")
				.hasFieldOrProperty("traceabilityId")
				.hasFieldOrProperty("responseCode")
				.hasFieldOrProperty("acquirerResponseCode")
				.hasFieldOrProperty("message");

		assertThat(responseMessage.getResponseCode())
				.as("The authorization must have the same ResponseCode as specified on the payer name")
				.isNotNull()
				.isEqualTo(expectedAuthorizationResponseCode);

	}

	/**
	 * Creates a sample Authorization Request message
	 *
	 * @return A new Authorization Request sample message
	 */
	private AuthorizationRequestMessage.Builder createSampleAuthorizationRequest() {

		return AuthorizationRequestMessage
				.withCorrelationId(UUID.randomUUID().toString())
				.withMerchant(MerchantDto
						.withId(100)
						.withMerchantCategoryCode("9977")
						.withName("Merchant Name")
						.build())
				.withAmount(AmountDto
						.of("USD", BigDecimal.valueOf(10.50))
						.withDetail(AmountDetail.of(BigDecimal.valueOf(1.50)).build())
						.build())
				.withOrder(OrderDto
						.withId(123)
						.withReferenceCode("Ref-123456")
						.build())
				.withRoute(RouteDto
						.withId(UUID.randomUUID().toString())
						.addExtraParameter("Key1", "Value1")
						.build())
				.addPaymentInstrument("PAYMENT_TYPE", "CREDIT_CARD")
				.addPaymentInstrument("PAYMENT_BRAND", "VISA")
				.addPaymentInstrument("CARD_PAN", "4242424242424242")
				.addPaymentInstrument("CARD_EXPIRATION_DATE", "2025/12")
				.addPaymentInstrument("CARD_SECURITY_CODE", "000")
				.addPaymentInstrument("INSTALLMENTS", "1");
	}

}
