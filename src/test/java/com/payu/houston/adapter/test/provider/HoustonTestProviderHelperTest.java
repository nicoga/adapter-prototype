/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.provider;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseCode;
import com.payu.houston.adapter.queue.model.common.RequestMessage;
import com.payu.houston.adapter.test.AccessModifierTestUtil;
import org.testng.annotations.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test for {@linkplain HoustonTestProviderHelper} class
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class HoustonTestProviderHelperTest {

	/**
	 * Test case for {@linkplain HoustonTestProviderHelper#addDelay addDelay} method.
	 * <p>
	 * Must not include any delay if the request message does not have any payment instrument defined
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 7/07/2016
	 */
	@Test
	public void testAddDelayNoPaymentInstruments() {

		final RequestMessage message = AuthorizationRequestMessage
				.withCorrelationId("123")
				.build();

		final Instant start = Instant.now();
		HoustonTestProviderHelper.addDelay(message);
		final Instant stop = Instant.now();

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isLessThan(Duration.of(1, ChronoUnit.SECONDS));

	}

	/**
	 * Test case for {@linkplain HoustonTestProviderHelper#addDelay addDelay} method.
	 * <p>
	 * Must not include any delay since the credit card security code does not start with 5
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testAddDelayMethodNoDelayConfigured() {

		final RequestMessage message = AuthorizationRequestMessage
				.withCorrelationId("123")
				.addPaymentInstrument("CARD_PAN", "4242424242424242")
				.addPaymentInstrument("CARD_SECURITY_CODE", "123")
				.addPaymentInstrument("CARD_EXPIRATION_DATE", "2025/112")
				.build();

		final Instant start = Instant.now();
		HoustonTestProviderHelper.addDelay(message);
		final Instant stop = Instant.now();

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isLessThan(Duration.of(1, ChronoUnit.SECONDS));

	}

	/**
	 * Test case for {@linkplain HoustonTestProviderHelper#addDelay addDelay} method.
	 * <p>
	 * Must include a delay defined on the card security code numbers
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testAddDelayOf3Seconds() {

		final RequestMessage message = AuthorizationRequestMessage
				.withCorrelationId("123")
				.addPaymentInstrument("CARD_PAN", "4242424242424242")
				.addPaymentInstrument("CARD_SECURITY_CODE", "503")
				.addPaymentInstrument("CARD_EXPIRATION_DATE", "2025/112")
				.build();

		final Instant start = Instant.now();
		HoustonTestProviderHelper.addDelay(message);
		final Instant stop = Instant.now();

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isGreaterThanOrEqualTo(Duration.of(3, ChronoUnit.SECONDS))
				.isLessThan(Duration.of(4, ChronoUnit.SECONDS));

	}

	/**
	 * Test case for {@linkplain HoustonTestProviderHelper#addDelay addDelay} method.
	 * <p>
	 * Must not include any delay or generate any error even for invalid security code
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testAddDelayMethodWithInvalidSecurityCode() {

		final RequestMessage message = AuthorizationRequestMessage
				.withCorrelationId("123")
				.addPaymentInstrument("CARD_PAN", "4242424242424242")
				.addPaymentInstrument("CARD_SECURITY_CODE", "5A0")
				.addPaymentInstrument("CARD_EXPIRATION_DATE", "2025/112")
				.build();

		final Instant start = Instant.now();
		HoustonTestProviderHelper.addDelay(message);
		final Instant stop = Instant.now();

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isLessThan(Duration.of(1, ChronoUnit.SECONDS));

	}

	/**
	 * Test case for {@linkplain HoustonTestProviderHelper#getAuthResponseCode(String)} method
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void testGetAuthResponseCodeMethod() {

		assertThat(HoustonTestProviderHelper.getAuthResponseCode("APPROVED"))
				.isEqualTo(AuthorizationResponseCode.APPROVED);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode("NETWORK_APPROVED"))
				.isEqualTo(AuthorizationResponseCode.APPROVED);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode("ENTITY_APPROVED"))
				.isEqualTo(AuthorizationResponseCode.APPROVED);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode("NETWORK_DECLINED"))
				.isEqualTo(AuthorizationResponseCode.PAYMENT_NETWORK_REJECTED);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode("ENTITY_DECLINED"))
				.isEqualTo(AuthorizationResponseCode.ENTITY_DECLINED);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode("NETWORK_BAD_RESPONSE"))
				.isEqualTo(AuthorizationResponseCode.PAYMENT_NETWORK_BAD_RESPONSE);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode("NETWORK_NO_RESPONSE"))
				.isEqualTo(AuthorizationResponseCode.PAYMENT_NETWORK_NO_RESPONSE);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode("NETWORK_ERROR"))
				.isEqualTo(AuthorizationResponseCode.ERROR);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode("ERROR"))
				.isEqualTo(AuthorizationResponseCode.ERROR);

		assertThat(HoustonTestProviderHelper.getAuthResponseCode(""))
				.isEqualTo(AuthorizationResponseCode.APPROVED);
		assertThat(HoustonTestProviderHelper.getAuthResponseCode(null))
				.isEqualTo(AuthorizationResponseCode.APPROVED);

	}

	/**
	 * Test private access modifier of the class
	 *
	 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
	 * @date 12/07/2016
	 */
	@Test
	public void test() {

		AccessModifierTestUtil.testUtilityClassConstructor(HoustonTestProviderHelper.class);

	}

}