/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@linkplain JmsConfigurator} class
 *
 * @author Santiago Alzate S. (santiago.alzate@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class JmsConfigurationMockTest {

	/**
	 * JMS Configurator (Class under testing)
	 */
	private JmsConfigurator configurator;

	/**
	 * Set up test environment before each test
	 */
	@BeforeTest
	public void setUp() {

		configurator = new JmsConfigurator();
		configurator.setPreserveMessageQos(false);
		configurator.setConcurrentConsumers(1);
		configurator.setMaxConcurrentConsumers(5);
		configurator.setMaxThreadPoolSize(5);
		configurator.setMaxMessagesPerTask(100);
		configurator.setAsyncConsumer(false);
		configurator.setDisableReplyTo(false);
		configurator.setDeliveryPersistent(true);
		configurator.setTimeToLive(5000);
		configurator.setCopyMessageOnSend(false);
		configurator.setUseCompression(true);
		configurator.setMaxThreadPoolSize(300);
		configurator.setMessagePrioritySupported(false);
		configurator.setDispatchAsync(false);
		configurator.setUseAsyncSend(false);
		configurator.setStatsEnabled(false);
	}

	/**
	 * Test the creation of the Connection Factory used to send and receive messages from the broker
	 *
	 * @author santiago.alzate
	 * @throws Exception 
	 * @date 06/06/2017
	 */
	@Test
	public void testJmsConnectionFactoryCreation() throws Exception {

		final BrokerConfigurator broker = new BrokerConfigurator();
		broker.setBrokerUrl("vm://localhost");
		assertThat(configurator.getConnectionFactory(broker))
				.isNotNull()
				.satisfies(fact -> assertThat(fact.getConnectionFactory()).isNotNull());

	}

	/**
	 * Test the creation of the JMS Component used to send and receive messages from the broker
	 *
	 * @author manuel.vieda
	 * @throws Exception 
	 * @date 30/08/2016
	 */
	@Test
	public void testJmsComponentCreation() throws Exception {

		final BrokerConfigurator broker = new BrokerConfigurator();
		broker.setBrokerUrl("vm://localhost");
		assertThat(configurator.getJmsComponent(broker.buildSslConnectionFactory()))
				.isNotNull()
				.satisfies(comp -> assertThat(comp.getConfiguration()).isNotNull());

	}

	/**
	 * Test set and get methods
	 *
	 * @author manuel.vieda
	 * @date 30/08/2016
	 */
	@Test
	public void testSetAndGetProperties() {

		assertThat(configurator.isPreserveMessageQos()).isFalse();
		assertThat(configurator.getConcurrentConsumers()).isNotNull().isEqualTo(1);
		assertThat(configurator.getMaxConcurrentConsumers()).isNotNull().isEqualTo(5);
		assertThat(configurator.getMaxMessagesPerTask()).isNotNull().isEqualTo(100);
		assertThat(configurator.isAsyncConsumer()).isFalse();
		assertThat(configurator.isDisableReplyTo()).isFalse();
		assertThat(configurator.isDeliveryPersistent()).isTrue();
		assertThat(configurator.getTimeToLive()).isNotNull().isEqualTo(5000);
		assertThat(configurator.isCopyMessageOnSend()).isFalse();
		assertThat(configurator.isUseCompression()).isTrue();
		assertThat(configurator.getMaxThreadPoolSize()).isNotNull().isEqualTo(300);
		assertThat(configurator.isMessagePrioritySupported()).isFalse();
		assertThat(configurator.isDispatchAsync()).isFalse();
		assertThat(configurator.isUseAsyncSend()).isFalse();
		assertThat(configurator.isStatsEnabled()).isFalse();
	}

}
