/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.service.impl;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.common.AmountDto;
import com.payu.houston.adapter.queue.model.common.OrderDto;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.test.client.MockedNetworkClient;
import com.payu.houston.adapter.test.exception.MockedNetworkException;
import com.payu.houston.adapter.test.model.CreditCard;
import com.payu.houston.adapter.test.model.Merchant;
import com.payu.houston.adapter.test.model.NetworkResponse;
import com.payu.houston.adapter.test.model.Order;
import com.payu.houston.adapter.test.model.constant.ParametersName;
import com.payu.houston.adapter.test.model.request.NetworkAuthorizationRequest;
import com.payu.houston.adapter.test.model.request.NetworkCaptureRequest;
import com.payu.houston.adapter.test.model.request.NetworkRefundRequest;
import com.payu.houston.adapter.test.model.request.NetworkVoidRequest;
import com.payu.houston.adapter.test.service.MockedNetworkRequestFactory;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

/**
 * Test cases for the {@link NetworkServiceImpl} class
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(MockedNetworkRequestFactory.class)
@NoArgsConstructor
class NetworkServiceImplTest {

	/**
	 * Instance of tested class.
	 */
	@InjectMocks
	private NetworkServiceImpl networkService;

	/**
	 * Client to consume Mocked network used in {@linkplain NetworkServiceImpl}
	 */
	@Mock
	private MockedNetworkClient mockedNetworkClient;

	/**
	 * Request message used in test cases.
	 */
	private AuthorizationRequestMessage authorizationRequestMessage;

	/**
	 * Mocked network authorization request mock.
	 */
	private NetworkAuthorizationRequest networkAuthorizationRequest;

	/**
	 * Successful network authorization response.
	 */
	private NetworkResponse authorizationNetworkResponse;

	/**
	 * Request message used in capture test cases.
	 */
	private CaptureRequestMessage captureRequestMessage;

	/**
	 * Mocked network authorization request mock.
	 */
	private NetworkCaptureRequest networkCaptureRequest;

	/**
	 * Successful network authorization response.
	 */
	private NetworkResponse captureNetworkResponse;

	/**
	 * Request message used in void test cases.
	 */
	private VoidRequestMessage voidRequestMessage;

	/**
	 * Mocked network void request mock.
	 */
	private NetworkVoidRequest networkVoidRequest;

	/**
	 * Successful network void response.
	 */
	private NetworkResponse voidNetworkResponse;

	/**
	 * Request message used in refund test cases.
	 */
	private RefundRequestMessage refundRequestMessage;

	/**
	 * Mocked network refund request mock.
	 */
	private NetworkRefundRequest networkRefundRequest;

	/**
	 * Successful network refund response.
	 */
	private NetworkResponse refundNetworkResponse;

	@Before
	public void beforeEach() {

		authorizationRequestMessage = AuthorizationRequestMessage.withCorrelationId("123456").withOrder(
				OrderDto.withId(1234455).build()).withAmount(AmountDto.of("COP", new BigDecimal(200000)).build())
				.addPaymentInstrument(ParametersName.CARD_NUMBER.name(), "1234567890123").build();

		networkAuthorizationRequest = NetworkAuthorizationRequest.builder()
				.creditCard(CreditCard.builder().cardNumber("1234567890123").build()).order(
						Order.builder().orderId("1234455").amount("200000").currency("COP").build()).build();
		authorizationNetworkResponse = NetworkResponse.builder()
				.amount(networkAuthorizationRequest.getOrder().getAmount())
				.state("ACCEPTED").responseCode("0").transactionCode("AUTHORIZED").orderId("1111111")
				.responseMessage("Operation Successful").transactionDate(new Date().toString()).build();

		captureRequestMessage = CaptureRequestMessage.withCorrelationId("12345").withAmount(
				AmountDto.of("COP", new BigDecimal(20000)).build()).withAuthorizationTraceabilityIdentifier("1111")
				.build();

		networkCaptureRequest = NetworkCaptureRequest.builder().captureAmount("20000.00")
				.orderId("1111").merchant(Merchant.builder().apiKey(null).apiLogin(null).build()).build();

		captureNetworkResponse = NetworkResponse.builder().amount("200000").orderId("11111")
				.responseCode("0")
				.transactionDate(new Date().toString()).responseMessage("Approved transaction").state("ACCEPTED")
				.transactionCode("CAPTURED").build();

		voidRequestMessage = VoidRequestMessage.withCorrelationId("12345")
				.withAuthorizationTraceabilityIdentifier("1111")
				.build();

		networkVoidRequest = NetworkVoidRequest.builder()
				.orderId("1111").merchant(Merchant.builder().apiKey(null).apiLogin(null).build()).build();

		voidNetworkResponse = NetworkResponse.builder().orderId("11111")
				.responseCode("0")
				.transactionDate(new Date().toString()).responseMessage("Approved transaction").state("ACCEPTED")
				.transactionCode("VOIDED").build();

		refundRequestMessage = RefundRequestMessage.withCorrelationId("12345")
				.withCaptureTraceabilityId("1111").withAmount(AmountDto.of("COP", new BigDecimal(20000)).build())
				.build();

		networkRefundRequest = NetworkRefundRequest.builder()
				.orderId("1111").merchant(Merchant.builder().apiKey(null).apiLogin(null).build()).refundAmount("20000")
				.reason("Merchant requested").build();

		refundNetworkResponse = NetworkResponse.builder().orderId("11111")
				.responseCode("0")
				.transactionDate(new Date().toString()).responseMessage("Approved transaction").state("ACCEPTED")
				.transactionCode("REFUNDED").build();

	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doAuthorization} method successfully return a network response.
	 */
	@Test
	@SneakyThrows
	public void doAuthorizationShouldReturnAMockResponse() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createAuthorizationRequest(any(AuthorizationRequestMessage.class)))
				.thenReturn(networkAuthorizationRequest);
		when(mockedNetworkClient.doAuthorization(any(NetworkAuthorizationRequest.class))).thenReturn(
				authorizationNetworkResponse);
		Assert.assertEquals(authorizationNetworkResponse, networkService.doAuthorization(authorizationRequestMessage));
		verify(mockedNetworkClient).doAuthorization(any(NetworkAuthorizationRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createAuthorizationRequest(any(AuthorizationRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doAuthorization} handles correctly a {@linkplain MockedNetworkException}.
	 */
	@Test
	@SneakyThrows
	public void doAuthorizationShouldHandleMockedNetworkException() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createAuthorizationRequest(any(AuthorizationRequestMessage.class)))
				.thenReturn(networkAuthorizationRequest);
		when(mockedNetworkClient.doAuthorization(any(NetworkAuthorizationRequest.class)))
				.thenThrow(new MockedNetworkException("Error",
						NetworkResponse.builder().errorDetails("Insufficient funds.").build()));
		Assert.assertEquals(NetworkResponse.builder().errorDetails("Insufficient funds.").build(),
				networkService.doAuthorization(authorizationRequestMessage));
		verify(mockedNetworkClient).doAuthorization(any(NetworkAuthorizationRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createAuthorizationRequest(any(AuthorizationRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doAuthorization} handles correctly a {@linkplain Exception}.
	 */
	@Test
	@SneakyThrows
	public void doAuthorizationShouldHandleException() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createAuthorizationRequest(any(AuthorizationRequestMessage.class)))
				.thenReturn(networkAuthorizationRequest);
		when(mockedNetworkClient.doAuthorization(any(NetworkAuthorizationRequest.class)))
				.thenThrow(new NullPointerException());
		Assert.assertEquals(NetworkResponse.builder().state("ERROR")
						.errorDetails("An error occurred in the client while doing the request").build(),
				networkService.doAuthorization(authorizationRequestMessage));
		verify(mockedNetworkClient).doAuthorization(any(NetworkAuthorizationRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createAuthorizationRequest(any(AuthorizationRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doCapture} method successfully return a network response.
	 */
	@Test
	@SneakyThrows
	public void doCaptureShouldReturnMockResponse() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createCaptureRequest(any(CaptureRequestMessage.class)))
				.thenReturn(networkCaptureRequest);
		when(mockedNetworkClient.doCapture(any(NetworkCaptureRequest.class))).thenReturn(
				captureNetworkResponse);
		Assert.assertEquals(captureNetworkResponse, networkService.doCapture(captureRequestMessage));
		verify(mockedNetworkClient).doCapture(any(NetworkCaptureRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createCaptureRequest(any(CaptureRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doCapture} handles correctly a {@linkplain MockedNetworkException}.
	 */
	@Test
	@SneakyThrows
	public void doCaptureShouldHandleMockedNetworkException() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createCaptureRequest(any(CaptureRequestMessage.class)))
				.thenReturn(networkCaptureRequest);
		when(mockedNetworkClient.doCapture(any(NetworkCaptureRequest.class)))
				.thenThrow(new MockedNetworkException("Error",
						NetworkResponse.builder().errorDetails("Already captured.").build()));
		Assert.assertEquals(NetworkResponse.builder().errorDetails("Already captured.").build(),
				networkService.doCapture(captureRequestMessage));
		verify(mockedNetworkClient).doCapture(any(NetworkCaptureRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createCaptureRequest(any(CaptureRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doCapture} handles correctly a {@linkplain Exception}.
	 */
	@Test
	@SneakyThrows
	public void doACaptureShouldHandleException() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createCaptureRequest(any(CaptureRequestMessage.class)))
				.thenReturn(networkCaptureRequest);
		when(mockedNetworkClient.doCapture(any(NetworkCaptureRequest.class))).thenThrow(
				new NullPointerException());
		Assert.assertEquals(NetworkResponse.builder().state("ERROR")
						.errorDetails("An error occurred in the client while doing the request").build(),
				networkService.doCapture(captureRequestMessage));
		verify(mockedNetworkClient).doCapture(any(NetworkCaptureRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createCaptureRequest(any(CaptureRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doVoid} method successfully return a network response.
	 */
	@Test
	@SneakyThrows
	public void doVoidShouldReturnMockResponse() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createVoidRequest(any(VoidRequestMessage.class)))
				.thenReturn(networkVoidRequest);
		when(mockedNetworkClient.doVoid(any(NetworkVoidRequest.class))).thenReturn(
				voidNetworkResponse);
		Assert.assertEquals(voidNetworkResponse, networkService.doVoid(voidRequestMessage));
		verify(mockedNetworkClient).doVoid(any(NetworkVoidRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createVoidRequest(any(VoidRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doVoid} handles correctly a {@linkplain MockedNetworkException}.
	 */
	@Test
	@SneakyThrows
	public void doVoidShouldHandleMockedNetworkException() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createVoidRequest(any(VoidRequestMessage.class)))
				.thenReturn(networkVoidRequest);
		when(mockedNetworkClient.doVoid(any(NetworkVoidRequest.class)))
				.thenThrow(new MockedNetworkException("Error",
						NetworkResponse.builder().errorDetails("Already voided.").build()));
		Assert.assertEquals(NetworkResponse.builder().errorDetails("Already voided.").build(),
				networkService.doVoid(voidRequestMessage));
		verify(mockedNetworkClient).doVoid(any(NetworkVoidRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createVoidRequest(any(VoidRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doVoid} handles correctly a {@linkplain Exception}.
	 */
	@Test
	@SneakyThrows
	public void doVoidShouldHandleException() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createVoidRequest(any(VoidRequestMessage.class)))
				.thenReturn(networkVoidRequest);
		when(mockedNetworkClient.doVoid(any(NetworkVoidRequest.class))).thenThrow(
				new NullPointerException());
		Assert.assertEquals(NetworkResponse.builder().state("ERROR")
						.errorDetails("An error occurred in the client while doing the request").build(),
				networkService.doVoid(voidRequestMessage));
		verify(mockedNetworkClient).doVoid(any(NetworkVoidRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createVoidRequest(any(VoidRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doRefund} method successfully return a network response.
	 */
	@Test
	@SneakyThrows
	public void doRefundShouldReturnMockResponse() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createRefundRequest(any(RefundRequestMessage.class)))
				.thenReturn(networkRefundRequest);
		when(mockedNetworkClient.doRefund(any(NetworkRefundRequest.class))).thenReturn(
				refundNetworkResponse);
		Assert.assertEquals(refundNetworkResponse, networkService.doRefund(refundRequestMessage));
		verify(mockedNetworkClient).doRefund(any(NetworkRefundRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createRefundRequest(any(RefundRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doRefund} handles correctly a {@linkplain MockedNetworkException}.
	 */
	@Test
	@SneakyThrows
	public void doRefundShouldHandleMockedNetworkException() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createRefundRequest(any(RefundRequestMessage.class)))
				.thenReturn(networkRefundRequest);
		when(mockedNetworkClient.doRefund(any(NetworkRefundRequest.class)))
				.thenThrow(new MockedNetworkException("Error",
						NetworkResponse.builder().errorDetails("Already refunded.").build()));
		Assert.assertEquals(NetworkResponse.builder().errorDetails("Already refunded.").build(),
				networkService.doRefund(refundRequestMessage));
		verify(mockedNetworkClient).doRefund(any(NetworkRefundRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createRefundRequest(any(RefundRequestMessage.class));
	}

	/**
	 * Tests that {@linkplain NetworkServiceImpl#doRefund} handles correctly a {@linkplain Exception}.
	 */
	@Test
	@SneakyThrows
	public void doRefundShouldHandleException() {

		mockStatic(MockedNetworkRequestFactory.class);
		when(MockedNetworkRequestFactory.createRefundRequest(any(RefundRequestMessage.class)))
				.thenReturn(networkRefundRequest);
		when(mockedNetworkClient.doRefund(any(NetworkRefundRequest.class))).thenThrow(
				new NullPointerException());
		Assert.assertEquals(NetworkResponse.builder().state("ERROR")
						.errorDetails("An error occurred in the client while doing the request").build(),
				networkService.doRefund(refundRequestMessage));
		verify(mockedNetworkClient).doRefund(any(NetworkRefundRequest.class));
		verifyStatic(MockedNetworkRequestFactory.class);
		MockedNetworkRequestFactory.createRefundRequest(any(RefundRequestMessage.class));
	}
}