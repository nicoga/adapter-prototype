/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import com.payu.houston.adapter.test.application.GenericFlow;
import org.apache.camel.model.FromDefinition;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.model.RoutesDefinition;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Provides basic test methods for the {@link GenericFlow} subclasses.
 *
 * @author Santiago Alzate S. (santiago.alzate@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public abstract class GenericFlowMockTest {

	/**
	 * Generic flow being testes.
	 */
	private GenericFlow genericFlow;

	/**
	 * Test queue concurrent consumers.
	 */
	private static final Integer CONCURRENT_CONSUMERS = 10;

	/**
	 * Test queue maximum concurrent consumers.
	 */
	private static final Integer MAX_CONCURRENT_CONSUMERS = 50;

	/**
	 * Test request queue name.
	 */
	private static final String REQUEST_QUEUE_NAME = "aa.request.queue.name";

	/**
	 * Test response queue name.
	 */
	private static final String RESPONSE_QUEUE_NAME = "aa.response.queue.name";

	/**
	 * Set up the generic flow for testing
	 *
	 * @param concreteGenericFlow The concrete implementation of the flow/route being configured for testing
	 */
	public void setUp(final GenericFlow concreteGenericFlow) {

		genericFlow = concreteGenericFlow;
		genericFlow.setConcurrentConsumers(CONCURRENT_CONSUMERS);
		genericFlow.setMaxConcurrentConsumers(MAX_CONCURRENT_CONSUMERS);
		genericFlow.setRequestQueueName(REQUEST_QUEUE_NAME);
		genericFlow.setResponseQueueName(RESPONSE_QUEUE_NAME);
		genericFlow.getResponseQueueName();
		genericFlow.getRequestMessageClass();
	}

	/**
	 * Test the route configuration for Apache Camel
	 *
	 * @author Santiago Alzate S. (santiago.alzate@payulatam.com)
	 * @date 27/07/2016
	 */
	@Test
	public void testConfigureFlow() {

		try {
			genericFlow.configure();

			final RoutesDefinition routeCollection = genericFlow.getRouteCollection();
			assertThat(routeCollection).isNotNull();
			assertThat(routeCollection.getRoutes())
					.isNotNull()
					.hasSize(1);

			routeCollection.getRoutes().forEach(System.out::println);

			final RouteDefinition routeDefinition = routeCollection.getRoutes().get(0);

			assertThat(routeDefinition.getInputs())
					.isNotEmpty()
					.hasSize(1)
					.doesNotContainNull();

			final FromDefinition fromDefinition = routeDefinition.getInputs().get(0);
			assertThat(fromDefinition.getEndpointUri())
					.isNotEmpty()
					.containsSequence(
							"activemq:queue:",
							genericFlow.getRequestQueueName(),
							"concurrentConsumers=",
							genericFlow.getConcurrentConsumers().toString(),
							"maxConcurrentConsumers=",
							genericFlow.getMaxConcurrentConsumers().toString());

			assertThat(routeDefinition.getOutputs())
					.isNotEmpty()
					.doesNotContainNull();

		} catch (final Exception e) {
			Assertions.fail("Unexpected error during Camel route configuration test", e);
		}

	}

}
