/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application;

import com.payu.houston.adapter.queue.model.common.AmountDetail;
import com.payu.houston.adapter.queue.model.common.AmountDto;
import com.payu.houston.adapter.queue.model.common.RouteDto;
import com.payu.houston.adapter.queue.model.query.QueryInstrumentKey;
import com.payu.houston.adapter.queue.model.query.QueryRequestMessage;
import com.payu.houston.adapter.queue.model.query.QueryResponseCode;
import com.payu.houston.adapter.queue.model.query.QueryResponseMessage;
import com.payu.houston.adapter.queue.model.query.operation.OperationRequestMessage;
import com.payu.houston.adapter.queue.model.query.operation.OperationType;
import com.payu.houston.adapter.test.JmsIntegrationTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test suite for Query operation
 *
 * @author David Hidalgo (david.hidalgo@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class QueryOperationIntegrationTest extends JmsIntegrationTest {

	private static String QUERY_QUEUE_PREFIX = "aa.query.";

	/**
	 * Test a Query operation against the Test Adapter, with a any destination
	 *
	 * @author David Hidalgo (david.hidalgo@payulatam.com)
	 * @date 22/07/2016
	 */
	@Test(enabled = false)
	public void testSendAndReceiveQueryAnyDestination() {

		final QueryRequestMessage request = createSampleQueryRequest().build();
		assertThat(request).isNotNull();

		final Instant start = Instant.now();

		String destinationName = QUERY_QUEUE_PREFIX + UUID.randomUUID().toString();
		sendMessageAndReplyTo(request, destinationName);
		
		final QueryResponseMessage responseMessage = receiveQueryResponseMessage(destinationName);

		final Instant stop = Instant.now();
		assertThat(responseMessage)
				.as("The response message for the adapter must generate a complete response")
				.isNotNull();
		
		assertThat(responseMessage.getQueryResponseCode())
				.as("The query response must be not null")
				.isNotNull();

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isLessThan(Duration.of(5, ChronoUnit.SECONDS));
	}
	
	/**
	 * Test a Query operation against the Test Adapter, with a success response
	 *
	 * @author David Hidalgo (david.hidalgo@payulatam.com)
	 * @date 22/07/2016
	 */
	@Test
	public void testSendAndReceiveQuerySuccessResponse() {

		final QueryRequestMessage request = createSampleQueryRequest()
				.addQueryInstrument(QueryInstrumentKey.CARD_PAN, "411111111111111").build();
		assertThat(request).isNotNull();

		final Instant start = Instant.now();

		sendMessage(request);
		final QueryResponseMessage responseMessage = receiveQueryResponseMessage();

		final Instant stop = Instant.now();
		assertThat(responseMessage)
				.as("The response message for the adapter must generate a complete response")
				.isNotNull();

		assertThat(responseMessage.getQueryResponseCode())
				.as("The query response must be SUCCESS").isNotNull()
				.isEqualTo(QueryResponseCode.SUCCESS);

		assertThat(responseMessage.getOperationResponse())
				.as("The response message operation for the adapter must generate a complete response")
				.isNotNull()
				.hasFieldOrProperty("correlationId")
				.hasFieldOrProperty("traceabilityId")
				.hasFieldOrProperty("responseCode")
				.hasFieldOrProperty("acquirerResponseCode")
				.hasFieldOrProperty("message");

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isLessThan(Duration.of(5, ChronoUnit.SECONDS));
	}

	/**
	 * Test a Query operation against the Test Adapter, with error response
	 *
	 * @author David Hidalgo (david.hidalgo@payulatam.com)
	 * @date 22/07/2016
	 */
	@Test
	public void testSendAndReceiveQueryErrorResponse() {

		final QueryRequestMessage request = createSampleQueryRequest()
				.addQueryInstrument(QueryInstrumentKey.CARD_PAN, "5105105105105100").build();
		assertThat(request).isNotNull();

		final Instant start = Instant.now();

		sendMessage(request);
		final QueryResponseMessage responseMessage = receiveQueryResponseMessage();

		final Instant stop = Instant.now();
		assertThat(responseMessage)
				.as("The response message for the adapter must generate a complete response")
				.isNotNull();

		assertThat(responseMessage.getQueryResponseCode())
				.as("The query response must be ERROR")
				.isNotNull()
				.isEqualTo(QueryResponseCode.ERROR);

		assertThat(responseMessage.getQueryResponseCode())
				.as("The query response code must be not null")
				.isNotNull();

		assertThat(responseMessage.getQueryErrorMessage())
				.as("The query error message must be not null")
				.isNotNull();

		assertThat(responseMessage.getOperationResponse())
				.as("The response message operation for the adapter must be null")
				.isNull();

		assertThat(Duration.between(start, stop))
				.as("Must not be any delay on the execution")
				.isLessThan(Duration.of(5, ChronoUnit.SECONDS));
	}

	/**
	 * Creates a sample Query Request message
	 *
	 * @return A new Query Request sample message
	 */
	private QueryRequestMessage.Builder createSampleQueryRequest() {

		final OperationRequestMessage operation = OperationRequestMessage.withId(UUID.randomUUID().toString())
				.withOperationType(OperationType.AUTHORIZATION)
				.withTraceabilityId(UUID.randomUUID().toString()).build();
		return QueryRequestMessage
				.ofOperationRequestMessage(operation)
				.withCorrelationId(UUID.randomUUID().toString())
				.withAmount(AmountDto
						.of("USD", BigDecimal.valueOf(10.50))
						.withDetail(AmountDetail.of(BigDecimal.valueOf(1.50)).build())
						.build())
				.withRoute(RouteDto
						.withId(UUID.randomUUID().toString())
						.addExtraParameter("Key1", "Value1")
						.build())
				.addQueryInstrument(QueryInstrumentKey.INSTALLMENTS, "1");
	}
}
