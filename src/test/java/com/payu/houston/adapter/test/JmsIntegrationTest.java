/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseMessage;
import com.payu.houston.adapter.queue.model.query.QueryRequestMessage;
import com.payu.houston.adapter.queue.model.query.QueryResponseMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidResponseMessage;
import com.payu.houston.adapter.test.application.TestAdapterApplication;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.util.ByteArrayOutputStream;
import org.apache.activemq.util.ByteSequence;
import org.apache.activemq.util.ByteSequenceData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeSuite;

import javax.jms.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.zip.Inflater;

/**
 * Abstract class that configures an ActiveMQ broker in embedded mode and offers the capability of sending a reading
 * message for integration tests.
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@SpringBootTest(
		classes = TestAdapterApplication.class,
		webEnvironment = SpringBootTest.WebEnvironment.NONE)
public abstract class JmsIntegrationTest extends AbstractTestNGSpringContextTests {

	/**
	 * Class logger
	 */
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
 
	/**
	 * The Active MQ broker url to connect to.
	 */
	private static final String BROKER_URL = "vm://embedded-broker?create=false";

	/**
	 * JMS Template default receive timeout
	 */
	public static final long QUEUE_RECEIVE_DEFAULT_TIMEOUT = 300_000L;

	/**
	 * The authorization queue name
	 */
	@Value("${test.authorization.requestQueueName}")
	private String authorizationRequestQueueName;

	/**
	 * The authorization response queue name
	 */
	@Value("${test.authorization.responseQueueName}")
	private String authorizationResponseQueueName;

	/**
	 * The capture queue name
	 */
	@Value("${test.capture.requestQueueName}")
	private String captureRequestQueueName;

	/**
	 * The capture response queue name
	 */
	@Value("${test.capture.responseQueueName}")
	private String captureResponseQueueName;

	/**
	 * The refund queue name
	 */
	@Value("${test.refund.requestQueueName}")
	private String refundRequestQueueName;

	/**
	 * The refund response queue name
	 */
	@Value("${test.refund.responseQueueName}")
	private String refundResponseQueueName;

	/**
	 * The void queue name
	 */
	@Value("${test.void.requestQueueName}")
	private String voidRequestQueueName;

	/**
	 * The void response queue name
	 */
	@Value("${test.void.responseQueueName}")
	private String voidResponseQueueName;

	/**
	 * The query queue name
	 */
	@Value("${test.query.requestQueueName}")
	private String queryRequestQueueName;

	/**
	 * The query response queue name
	 */
	@Value("${test.query.responseQueueName}")
	protected String queryResponseQueueName;

	/**
	 * Generic {@linkplain JmsTemplate} used to send and receive messages to/from the queues
	 */
	private static JmsTemplate genericJmsTemplate;

	/**
	 * Set upt test environment before class tests. This configures all the JMSTemplates
	 *
	 * @throws JMSException In case of error establishing connection with the queue
	 */
	@BeforeSuite(alwaysRun = true)
	public synchronized void setUpProducers() throws JMSException {

		ConnectionFactory connectionFactory;

		try {

			connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
			connectionFactory.createConnection();

		} catch (final JMSException jmsException) {

			final EmbeddedActiveMQ broker = new EmbeddedActiveMQ();
			broker.start();
			connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
			connectionFactory.createConnection();
		}

		genericJmsTemplate = new JmsTemplate();
		genericJmsTemplate.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		genericJmsTemplate.setMessageConverter(new GenericMessageConverter<>());
		genericJmsTemplate.setConnectionFactory(connectionFactory);
		genericJmsTemplate.setReceiveTimeout(QUEUE_RECEIVE_DEFAULT_TIMEOUT);

	}

	/**
	 * Send a {@linkplain AuthorizationRequestMessage} to queue
	 *
	 * @param authorizationRequestMessage The message to send
	 */
	protected void sendMessage(final AuthorizationRequestMessage authorizationRequestMessage) {

		log.trace("Send AuthorizationRequestMessage. QueueName: [{}], {}", authorizationRequestQueueName,
				authorizationRequestMessage);
		genericJmsTemplate.convertAndSend(authorizationRequestQueueName, authorizationRequestMessage);
	}

	/**
	 * Send a {@linkplain CaptureRequestMessage} to queue
	 *
	 * @param captureRequestMessage The message to send
	 */
	protected void sendMessage(final CaptureRequestMessage captureRequestMessage) {

		log.trace("Send CaptureRequestMessage. QueueName: [{}], {}", captureRequestQueueName, captureRequestMessage);
		genericJmsTemplate.convertAndSend(captureRequestQueueName, captureRequestMessage);
	}

	/**
	 * Send a {@linkplain RefundRequestMessage} to queue
	 *
	 * @param refundRequestMessage The message to send
	 */
	protected void sendMessage(final RefundRequestMessage refundRequestMessage) {

		log.trace("Send RefundRequestMessage. QueueName: [{}], {}", refundRequestQueueName, refundRequestMessage);
		genericJmsTemplate.convertAndSend(refundRequestQueueName, refundRequestMessage);
	}

	/**
	 * Send a {@linkplain VoidRequestMessage} to queue
	 *
	 * @param voidRequestMessage The message to send
	 */
	protected void sendMessage(final VoidRequestMessage voidRequestMessage) {

		log.trace("Send VoidRequestMessage. QueueName: [{}], {}", voidRequestQueueName, voidRequestMessage);
		genericJmsTemplate.convertAndSend(voidRequestQueueName, voidRequestMessage);
	}

	/**
	 * Send a {@linkplain QueryRequestMessage} to queue
	 *
	 * @param queryRequestMessage The message to send
	 * @param replyTo the destination to reply
	 */
	protected void sendMessageAndReplyTo(final QueryRequestMessage queryRequestMessage, final String replyTo) {

		log.trace("Send QueryRequestMessage. QueueName: [{}], {}", queryRequestQueueName, queryRequestMessage);
		sendMessageWithReply(genericJmsTemplate, queryRequestMessage, queryRequestQueueName, replyTo);
	}
	
	/**
	 * Send a {@linkplain VoidRequestMessage} to queue
	 *
	 * @param voidRequestMessage The message to send
	 */
	protected void sendMessage(final QueryRequestMessage queryRequestMessage) {

		log.trace("Send QueryRequestMessage. QueueName: [{}], {}", queryRequestQueueName, queryRequestMessage);
		genericJmsTemplate.convertAndSend(queryRequestQueueName, queryRequestMessage);
	}

	/**
	 * Read an {@linkplain AuthorizationResponseMessage} message from the queue. If can retrieve a message before the
	 * timeout, an unchecked {@link JmsException} is thrown.
	 *
	 * @return A message from the queue.
	 * @see JmsIntegrationTest#QUEUE_RECEIVE_DEFAULT_TIMEOUT
	 */
	protected AuthorizationResponseMessage receiveAuthorizationResponse() {

		return (AuthorizationResponseMessage) genericJmsTemplate.receiveAndConvert(authorizationResponseQueueName);
	}

	/**
	 * Read an {@linkplain CaptureResponseMessage} message from the queue. If can retrieve a message before the
	 * timeout, an unchecked {@link JmsException} is thrown.
	 *
	 * @return A message from the queue.
	 * @see JmsIntegrationTest#QUEUE_RECEIVE_DEFAULT_TIMEOUT
	 */
	protected CaptureResponseMessage receiveCaptureResponseMessage() {

		return (CaptureResponseMessage) genericJmsTemplate.receiveAndConvert(captureResponseQueueName);
	}

	/**
	 * Read an {@linkplain RefundResponseMessage} message from the queue. If can retrieve a message before the timeout,
	 * an unchecked {@link JmsException} is thrown.
	 *
	 * @return A message from the queue.
	 * @see JmsIntegrationTest#QUEUE_RECEIVE_DEFAULT_TIMEOUT
	 */
	protected RefundResponseMessage receiveRefundResponseMessage() {

		return (RefundResponseMessage) genericJmsTemplate.receiveAndConvert(refundResponseQueueName);
	}

	/**
	 * Read an {@linkplain VoidResponseMessage} message from the queue. If can retrieve a message before the timeout,
	 * an unchecked {@link JmsException} is thrown.
	 *
	 * @return A message from the queue.
	 * @see JmsIntegrationTest#QUEUE_RECEIVE_DEFAULT_TIMEOUT
	 */
	protected VoidResponseMessage receiveVoidResponseMessage() {

		return (VoidResponseMessage) genericJmsTemplate.receiveAndConvert(voidResponseQueueName);
	}

	/**
	 * Read an {@linkplain QueryResponseMessage} message from the queue. If can retrieve a message before the timeout,
	 * an unchecked {@link JmsException} is thrown.
	 *
	 * @return A message from the queue.
	 * @see JmsIntegrationTest#QUEUE_RECEIVE_DEFAULT_TIMEOUT
	 */
	protected QueryResponseMessage receiveQueryResponseMessage() {

		return (QueryResponseMessage) genericJmsTemplate.receiveAndConvert(queryResponseQueueName);
	}

	/**
	 * Read an {@linkplain VoidResponseMessage} message from the queue. If can retrieve a message before the timeout, an
	 * unchecked {@link JmsException} is thrown.
	 *
	 *@param destinationName the destination name 
	 *
	 * @return A message from the queue.
	 * @see JmsIntegrationTest#QUEUE_RECEIVE_DEFAULT_TIMEOUT
	 */
	protected QueryResponseMessage receiveQueryResponseMessage(final String destinationName) {

		return (QueryResponseMessage) genericJmsTemplate.receiveAndConvert(destinationName);
	}

	/**
	 * Send a Message with the given jmsTemplaten message and replayTo
	 * @param jmsTemplate a {@link JmsTemplate}
	 * @param message a {@link Object}
	 * @param replayTo a {@link String}
	 */
	private void sendMessageWithReply(final JmsTemplate jmsTemplate, final Object message, final String destination,
			final String replyTo) {

		jmsTemplate.send(destination, new JmsMessageCreator(message, replyTo));
	}

	/**
	 * Class for send messages with replay
	 * @author David Hidalgo (david.hidalgo@payulatam.com) 
	 * @version 1.0
	 * @since 1.0
	 */
	private class JmsMessageCreator implements MessageCreator, Serializable {
		private static final long serialVersionUID = 6177878457774517L;

		/**
		 * The message to send
		 */
		private final Object messageObject;
		
		/**
		 * Destination to replay
		 */
		private final String replyTo;
		
		/**
		 * The object mapper
		 */
		final ObjectMapper mapper = new ObjectMapper();

		/**
		 * Default {@link JmsMessageCreator} Constructor
 		 * @param messageObject
		 * @param replayTo
		 */
		public JmsMessageCreator(final Object messageObject,final String replyTo) {
			this.messageObject = messageObject;
			this.replyTo = replyTo;
		}

		@Override
		public Message createMessage(Session session) throws JMSException {
			
			final Message message;
			try {
				message = session.createTextMessage(mapper.writeValueAsString(messageObject));
			} catch (JsonProcessingException e) {
				throw new JMSException("Error to mapper the given object "+e.getMessage());
			}
			message.setJMSReplyTo(session.createQueue(replyTo));
			return message;
		}
	}
}

/**
 * Generic Message converter used to convert the JSON message body to the corresponding Java object
 *
 * @param <T> The message type
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 */
class GenericMessageConverter<T> implements MessageConverter {

	/**
	 * Object JSON mapper used for object serialization
	 */
	private static final ObjectMapper mapper = new ObjectMapper().registerModule(new Jdk8Module());

	/**
	 * Class logger
	 */
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * Generic Message constructor
	 */
	public GenericMessageConverter() {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Message toMessage(final Object object, final Session session)
			throws JMSException, MessageConversionException {

		try {
			return session.createTextMessage(mapper.writeValueAsString(object));
		} catch (final JsonProcessingException e) {
			log.error("Error sending  message to queue.", e);
			return null;
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T fromMessage(final Message message) throws JMSException, MessageConversionException {

		try {

			final String queueName = ((ActiveMQDestination) message.getJMSDestination()).getPhysicalName();

			final Class<?> type;

			if (queueName.contains("authorization")) {
				type = AuthorizationResponseMessage.class;
			} else if (queueName.contains("capture")) {
				type = CaptureResponseMessage.class;
			} else if (queueName.contains("refund")) {
				type = RefundResponseMessage.class;
			} else if (queueName.contains("void")) {
				type = VoidResponseMessage.class;
			} else if (queueName.contains("query")) {
				type = QueryResponseMessage.class;
			} else {
				throw new MessageConversionException("Cannot define response message type for JSON conversion");
			}

			final ActiveMQBytesMessage bytesMessage = (ActiveMQBytesMessage) message;
			final String messageBody = bytesMessage.isCompressed() ?
					decompress(bytesMessage.getContent()) :
					new String(bytesMessage.getContent().getData());
			log.trace("Message received From: [" + queueName + "], Body: [" + messageBody + "]");

			return mapper.readerFor(type).readValue(messageBody);

		} catch (final IOException e) {
			log.error("Error receiving message from queue. ", e);
			return null;
		}

	}

	/**
	 * Decompress JMS message
	 *
	 * @param pDataSequence Byte sequence
	 * @return A string containing the decompressed message
	 * @throws IOException
	 */
	private String decompress(final ByteSequence pDataSequence) throws IOException {

		final Inflater inflater = new Inflater();
		final ByteArrayOutputStream decompressed = new ByteArrayOutputStream();

		try {
			final ByteSequence dataSequence = new ByteSequence(
					pDataSequence.getData(),
					pDataSequence.getOffset(),
					pDataSequence.getLength());
			dataSequence.offset = 0;
			byte[] data = Arrays.copyOfRange(dataSequence.getData(), 4, dataSequence.getLength());
			inflater.setInput(data);
			byte[] buffer = new byte[ByteSequenceData.readIntBig(dataSequence)];
			int count = inflater.inflate(buffer);
			decompressed.write(buffer, 0, count);
			return new String(decompressed.toByteArray());

		} catch (Exception e) {
			throw new IOException(e);

		} finally {
			inflater.end();
			decompressed.close();
		}
	}

}
