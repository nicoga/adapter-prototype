/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.provider;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseCode;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseCode;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseMessage;
import com.payu.houston.adapter.queue.model.query.QueryErrorCode;
import com.payu.houston.adapter.queue.model.query.QueryRequestMessage;
import com.payu.houston.adapter.queue.model.query.QueryResponseMessage;
import com.payu.houston.adapter.queue.model.query.operation.OperationRequestMessage;
import com.payu.houston.adapter.queue.model.query.operation.OperationResponseCode;
import com.payu.houston.adapter.queue.model.query.operation.OperationResponseMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseCode;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidResponseCode;
import com.payu.houston.adapter.queue.model.voids.VoidResponseMessage;
import com.payu.houston.adapter.test.model.NetworkResponse;
import com.payu.houston.adapter.test.service.NetworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

/**
 * Test adapter for Houston
 *
 * @author Oscar Romero (oscar.romero@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Component
public class HoustonTestProvider {

	/**
	 * Default operation code used to create the response
	 */
	public static final String DEFAULT_OPERATION_CODE = "TEST123456789";

	/**
	 * Default traceability identifier used to create the response
	 */
	public static final String DEFAULT_TRACEABILITY_ID = "TEST_TRCID_123456";

	/**
	 * Default acquirer message used to create the response
	 */
	public static final String DEFAULT_ACQUIRER_MESSAGE = "Processed by TEST Adapter";

	/**
	 * Error to display when missing values in authorization message.
	 */
	private static final String AUTHORIZATION_ERROR_MESSAGE = "Payment instrument, order and amount values must be present and valid.";

	/**
	 * A specific extra parameter of the adapter
	 */
	private static final String TEST_ACQUIRER_TERMINAL = "TEST_ACQUIRER_TERMINAL";

	/**
	 * The class logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(HoustonTestProvider.class);

	/**
	 * The allow error response codes
	 */
	private static final QueryErrorCode[] ERROR_ALLOW_RESPONSE_CODES = { QueryErrorCode.PAYMENT_NETWORK_BAD_RESPONSE,
			QueryErrorCode.PAYMENT_NETWORK_NO_CONNECTION, QueryErrorCode.PAYMENT_NETWORK_NO_RESPONSE };

	/**
	 * A Random generator
	 */
	private static final SecureRandom random = new SecureRandom();

	/**
	 * Error to display when missing values in capture message.
	 */
	private static final String CAPTURE_ERROR_MESSAGE = "Authorization traceability identifier and captured amount values must be present and valid.";

	/**
	 * Error to display when missing values in void message.
	 */
	private static final String VOID_ERROR_MESSAGE = "Void traceability identifier value must be present and valid.";

	/**
	 * Error to display when missing values in refund message.
	 */
	private static final String REFUND_ERROR_MESSAGE = "Refund traceability identifier and total amount must be present and valid.";

	/**
	 * Mocked network service in order to process transactions.
	 */
	private final NetworkService networkService;

	@Autowired
	public HoustonTestProvider(final NetworkService networkService) {

		this.networkService = networkService;
	}

	/**
	 * Performs a fake request for the Authorize operation.
	 *
	 * @param request The request message
	 * @return Authorization response to be handled by the engine
	 */
	public AuthorizationResponseMessage doAuthorization(final AuthorizationRequestMessage request) {

		LOGGER.info("Processing authorization for attempt with correlation ID: [{}]", request.getCorrelationId());

		if (RequestValidator.validateAuthorizationRequestMessage(request)) {

			final NetworkResponse mockedNetworkResponse = networkService.doAuthorization(request);
			return ResponseFactory.createAuthorizationResponseMessage(request, mockedNetworkResponse);

		}

		LOGGER.warn("Authorization attempted with correlationId:[{}], contains null values, check request.",
				request.getCorrelationId());
		return AuthorizationResponseMessage
				.withCorrelationId(request.getCorrelationId())
				.withResponseCode(AuthorizationResponseCode.ERROR)
				.withErrorMessage(AUTHORIZATION_ERROR_MESSAGE)
				.build();
	}

	/**
	 * Performs a fake request for the Capture operation.
	 *
	 * @param request The request message
	 * @return Capture response to be handled by the engine
	 */
	public CaptureResponseMessage doCapture(final CaptureRequestMessage request) {

		LOGGER.info("Processing capture with correlation ID: [{}]", request.getCorrelationId());

		if (RequestValidator.validateCaptureRequestMessage(request)) {
			final NetworkResponse mockedNetworkResponse = networkService.doCapture(request);
			return ResponseFactory.createCaptureResponseMessage(request, mockedNetworkResponse);
		}

		LOGGER.warn("Capture attempted with correlationId:[{}], contains null values, check request.",
				request.getCorrelationId());
		return CaptureResponseMessage
				.withCorrelationId(request.getCorrelationId())
				.withResponseCode(CaptureResponseCode.ERROR)
				.withErrorMessage(CAPTURE_ERROR_MESSAGE)
				.build();
	}

	/**
	 * Performs a fake refund process
	 *
	 * @param request The refund request to process
	 * @return Refund response message to be handled by the houston capture module
	 */
	public RefundResponseMessage doRefund(final RefundRequestMessage request) {

		LOGGER.info("Processing refund with correlation ID: [{}]", request.getCorrelationId());
		if (RequestValidator.validateRefundRequest(request)) {
			final NetworkResponse mockedNetworkResponse = networkService.doRefund(request);
			return ResponseFactory.createRefundResponseMessage(request, mockedNetworkResponse);
		}

		LOGGER.warn("Refund attempted with correlationId:[{}], contains null values, check request.",
				request.getCorrelationId());

		return RefundResponseMessage
				.withCorrelationId(request.getCorrelationId())
				.withResponseCode(RefundResponseCode.ERROR)
				.withErrorMessage(REFUND_ERROR_MESSAGE)
				.build();
	}

	/**
	 * Performs a fake void process
	 *
	 * @param request The void request to process
	 * @return Void response message to be handled by the houston capture module
	 */
	public VoidResponseMessage doVoid(final VoidRequestMessage request) {

		LOGGER.info("Processing void with correlation ID: [{}]", request.getCorrelationId());

		if (RequestValidator.validateVoidRequestMessage(request)) {
			final NetworkResponse mockedNetworkResponse = networkService.doVoid(request);
			return ResponseFactory.createVoidResponseMessage(request, mockedNetworkResponse);
		}

		LOGGER.warn("Void attempted with correlationId:[{}], contains null values, check request.",
				request.getCorrelationId());

		return VoidResponseMessage
				.withCorrelationId(request.getCorrelationId())
				.withResponseCode(VoidResponseCode.ERROR)
				.withErrorMessage(VOID_ERROR_MESSAGE)
				.build();
	}

	/**
	 * Performs a fake query process
	 *
	 * @param request The query request to process
	 * @return Query response message to be handled by the houston query module
	 */
	public QueryResponseMessage doQuery(final QueryRequestMessage request) {

		LOGGER.info("Processing query with ID: [{}]", request.getCorrelationId());

		final String creditCard = request.getQueryInstrument() == null ? null
				: request.getQueryInstrument().get("CARD_PAN");
		final QueryResponseMessage result;
		boolean endsWithOne = creditCard != null && creditCard.endsWith("1");
		boolean endsWithZero = creditCard != null && creditCard.endsWith("0");

		if (!endsWithZero && (endsWithOne || random.nextBoolean())) {
			result = processResponse(request);
		} else {
			final QueryErrorCode errorCode = ERROR_ALLOW_RESPONSE_CODES[random
					.nextInt(ERROR_ALLOW_RESPONSE_CODES.length)];
			result = QueryResponseMessage.withErrorCode(errorCode)
					.withErrorMessage(errorCode.name() + " error has occurred")
					.build();

		}
		return result;
	}

	/**
	 * Creates a dummy response for the given query request
	 *
	 * @param request The query request to process
	 * @return A dummy/simulated query response
	 */
	private QueryResponseMessage processResponse(final QueryRequestMessage request) {

		final QueryResponseMessage result;
		try {
			final OperationRequestMessage operationRequest = request.getOperationRequest();
			final Map<String, String> queryResponseData = new HashMap<>();
			queryResponseData.put(TEST_ACQUIRER_TERMINAL, Integer.toString(random.nextInt(50)));

			final OperationResponseMessage responseOperation = OperationResponseMessage
					.withCorrelationId(operationRequest.getCorrelationId())
					.withResponseData(queryResponseData)
					.withTraceabilityId(operationRequest.getTraceabilityId())
					.withResponseCode(OperationResponseCode.APPROVED)
					.withAcquirerResponseCode(Integer.toString(random.nextInt(20)))
					.withMessage(DEFAULT_ACQUIRER_MESSAGE)
					.withOperationCode(DEFAULT_OPERATION_CODE).build();

			return QueryResponseMessage.withOperationResponseMessage(responseOperation)
					.withOperationType(operationRequest.getOperationType()).build();

		} catch (final RuntimeException e) {
			LOGGER.error("Error response building ", e);
			result = QueryResponseMessage.withErrorCode(QueryErrorCode.INVALID_PARAMETERS)
					.withErrorMessage(e.getMessage())
					.build();
		}

		return result;
	}

}
