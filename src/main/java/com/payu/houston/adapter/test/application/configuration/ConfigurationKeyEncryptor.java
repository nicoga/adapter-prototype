/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration;


import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.salt.StringFixedSaltGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class used to add encryption capabilities to the properties files with sensitive information,
 * replacing encrypted place-holders with the actual value in runtime.
 *
 * @author Manuel E. Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Configuration
public class ConfigurationKeyEncryptor {

	/**
	 * Name of the encryption algorithm used:
	 * <ul>
	 * <li><b>PBE</b> = password-based encryption,
	 * <li><b>WITHSHA256</b> = SHA-256, SHA (Secure Hash Algorithm), hash function that uses 32-bit words
	 * <li><b>AND256BITAES</b> = AES-256 (Advanced Encryption Standard) with 256 bits key length
	 * <li><b>CBC</b> = Cipher Block Chaining (CBC) mode for encryption. It is the default choice for turning a block
	 * cipher into a stream cipher. Stream ciphers are needed when one has a stream of data of arbitrarily length to
	 * be enciphered.
	 * <li><b>BC</b> = BouncyCastle as the cryptographic provider library
	 * </ul>
	 */
	private static final String PBE_STRING_ENCRYPTOR_ALGORITHM = "PBEWITHSHA256AND256BITAES-CBC-BC";

	/**
	 * Creates the PBE String encryptor (password-based encryption) for properties files
	 *
	 * @return a Standard PBE String Encryptor configured with the algorithm defined in the
	 * {@code PBE_STRING_ENCRYPTOR_ALGORITHM} variable
	 */
	@Bean(name = "jasyptStringEncryptor")
	public StandardPBEStringEncryptor standardPBEStringEncryptor(@Value("${POL_SAL}") final String payuSalt,
	                                                             @Value("${POL_ITERACIONES}") final String payuIter,
	                                                             @Value("${POL_KEY}") final String payuKey) {

		final PayUEnvironmentVariablesHelper helper =  new PayUEnvironmentVariablesHelper(payuSalt, payuIter, payuKey);
		final EnvironmentStringPBEConfig environmentStringPBEConfig = new EnvironmentStringPBEConfig();
		environmentStringPBEConfig.setProvider(new BouncyCastleProvider());
		environmentStringPBEConfig.setAlgorithm(PBE_STRING_ENCRYPTOR_ALGORITHM);
		environmentStringPBEConfig.setPassword(helper.getPayuKey());
		environmentStringPBEConfig.setKeyObtentionIterations(helper.getPayuIter());
		environmentStringPBEConfig.setSaltGenerator(new StringFixedSaltGenerator(helper.getPayuSalt()));

		final StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
		standardPBEStringEncryptor.setConfig(environmentStringPBEConfig);

		return standardPBEStringEncryptor;
	}
}
