/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.service;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.test.model.NetworkResponse;

/**
 * Service for network transactions.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public interface NetworkService {

	/**
	 * Process an authorization request in a network.
	 *
	 * @param request Authorization request to make.
	 * @return Network response.
	 */
	NetworkResponse doAuthorization(final AuthorizationRequestMessage request);

	/**
	 * Process a capture request in a network.
	 *
	 * @param request Capture request to make.
	 * @return Network response.
	 */
	NetworkResponse doCapture(final CaptureRequestMessage request);

	/**
	 * Process a void request in a network.
	 *
	 * @param request Void request to make.
	 * @return Network response.
	 */
	NetworkResponse doVoid(final VoidRequestMessage request);

	/**
	 * Process a refund request in a network.
	 *
	 * @param request Refund request to make.
	 * @return Network response.
	 */
	NetworkResponse doRefund(final RefundRequestMessage request);
}
