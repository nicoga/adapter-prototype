/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.exception;

import com.payu.houston.adapter.test.model.NetworkResponse;
import lombok.Getter;

/**
 * Exception definition for mocked network response errors.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Getter
public class MockedNetworkException extends Exception {

	/**
	 * Answer of Mock acquirer.
	 */
	private final NetworkResponse errorResponse;

	public MockedNetworkException(String message, NetworkResponse errorResponse) {

		super(message);
		this.errorResponse = errorResponse;
	}
}
