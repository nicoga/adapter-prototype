/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseCode;
import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseCode;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseCode;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidResponseCode;
import com.payu.houston.adapter.queue.model.voids.VoidResponseMessage;
import com.payu.houston.adapter.test.model.NetworkResponse;
import com.payu.houston.adapter.test.model.constant.TransactionResponseCodeMapper;

import java.util.Map;

/**
 * Class that creates the response based on the mocked network response.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class ResponseFactory {

	/**
	 * Creates an authorization response message based on a response of the network mock and the request made to it.
	 *
	 * @param request               Request made to the mocked network.
	 * @param mockedNetworkResponse Mocked network response.
	 * @return Mock network response parsed as a {@linkplain AuthorizationResponseMessage}
	 */
	public static AuthorizationResponseMessage createAuthorizationResponseMessage(
			final AuthorizationRequestMessage request,
			final NetworkResponse mockedNetworkResponse) {

		final AuthorizationResponseCode responseCode = TransactionResponseCodeMapper.
				buildFromNetworkCode(mockedNetworkResponse.getResponseCode()).getAuthorizationResponseCode();

		return AuthorizationResponseMessage.withCorrelationId(request.getCorrelationId())
				.withAuthorizationCode(mockedNetworkResponse.getState())
				.withTraceabilityId(mockedNetworkResponse.getOrderId())
				.withMessage(mockedNetworkResponse.getResponseMessage())
				.withAcquirerResponseCode(mockedNetworkResponse.getTransactionCode())
				.withResponseCode(responseCode)
				.withResponseData(mapResponseData(mockedNetworkResponse))
				.withErrorMessage(mockedNetworkResponse.getErrorDetails())
				.build();

	}

	/**
	 * Maps all the data of the mocked network response.
	 *
	 * @param mockedNetworkResponse Mocked network response.
	 * @return Response mapped data.
	 */
	private static Map<String, String> mapResponseData(final NetworkResponse mockedNetworkResponse) {

		ObjectMapper oMapper = new ObjectMapper();
		return oMapper.convertValue(mockedNetworkResponse, Map.class);
	}

	/**
	 * Creates a capture response message based on a response of the network mock and the request made to it.
	 *
	 * @param request               Request made to the mocked network.
	 * @param mockedNetworkResponse Mocked network response.
	 * @return Mock network response parsed as a {@linkplain CaptureResponseMessage}
	 */
	public static CaptureResponseMessage createCaptureResponseMessage(final CaptureRequestMessage request,
			final NetworkResponse mockedNetworkResponse) {

		final CaptureResponseCode responseCode = TransactionResponseCodeMapper
				.buildFromNetworkCode(mockedNetworkResponse.getResponseCode()).getCaptureResponseCode();
		return CaptureResponseMessage.withCorrelationId(request.getCorrelationId())
				.withCaptureCode(mockedNetworkResponse.getState())
				.withTraceabilityId(mockedNetworkResponse.getOrderId())
				.withMessage(mockedNetworkResponse.getResponseMessage())
				.withAcquirerResponseCode(mockedNetworkResponse.getTransactionCode())
				.withResponseCode(responseCode)
				.withResponseData(mapResponseData(mockedNetworkResponse))
				.withErrorMessage(mockedNetworkResponse.getErrorDetails())
				.build();
	}

	/**
	 * Creates a void response message based on a response of the network mock and the request made to it.
	 *
	 * @param request               Request made to the mocked network.
	 * @param mockedNetworkResponse Mocked network response.
	 * @return Mock network response parsed as a {@linkplain VoidResponseMessage}
	 */
	public static VoidResponseMessage createVoidResponseMessage(final VoidRequestMessage request,
			final NetworkResponse mockedNetworkResponse) {

		final VoidResponseCode responseCode = TransactionResponseCodeMapper
				.buildFromNetworkCode(mockedNetworkResponse.getResponseCode()).getVoidResponseCode();

		return VoidResponseMessage.withCorrelationId(request.getCorrelationId())
				.withVoidCode(mockedNetworkResponse.getState())
				.withTraceabilityId(mockedNetworkResponse.getOrderId())
				.withMessage(mockedNetworkResponse.getResponseMessage())
				.withAcquirerResponseCode(mockedNetworkResponse.getTransactionCode())
				.withResponseCode(responseCode)
				.withResponseData(mapResponseData(mockedNetworkResponse))
				.withErrorMessage(mockedNetworkResponse.getErrorDetails()).build();
	}

	/**
	 * Creates a refund response message based on a response of the network mock and the request made to it.
	 *
	 * @param request               Request made to the mocked network.
	 * @param mockedNetworkResponse Mocked network response.
	 * @return Mock network response parsed as a {@linkplain RefundResponseMessage}
	 */
	public static RefundResponseMessage createRefundResponseMessage(final RefundRequestMessage request,
			final NetworkResponse mockedNetworkResponse) {

		final RefundResponseCode responseCode = TransactionResponseCodeMapper
				.buildFromNetworkCode(mockedNetworkResponse.getResponseCode()).getRefundResponseCode();
		return RefundResponseMessage.withCorrelationId(request.getCorrelationId())
				.withRefundCode(mockedNetworkResponse.getState())
				.withTraceabilityId(mockedNetworkResponse.getOrderId())
				.withMessage(mockedNetworkResponse.getResponseMessage())
				.withAcquirerResponseCode(mockedNetworkResponse.getTransactionCode())
				.withResponseCode(responseCode)
				.withResponseData(mapResponseData(mockedNetworkResponse))
				.withErrorMessage(mockedNetworkResponse.getErrorDetails()).build();
	}
}
