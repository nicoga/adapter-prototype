/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.client;

import com.payu.houston.adapter.test.exception.MockedNetworkException;
import com.payu.houston.adapter.test.model.NetworkResponse;
import com.payu.houston.adapter.test.model.request.NetworkAuthorizationRequest;
import com.payu.houston.adapter.test.model.request.NetworkCaptureRequest;
import com.payu.houston.adapter.test.model.request.NetworkRefundRequest;
import com.payu.houston.adapter.test.model.request.NetworkVoidRequest;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Client that consumes the mocked network.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@FeignClient(url = "${mockClient.url}", name = "mockedNetworkClient")
public interface MockedNetworkClient {

	/**
	 * Consumes mocked network endpoint to process an authorization request.
	 *
	 * @param mockedNetworkRequest Authorization request to make to the network.
	 * @return Mocked network response.
	 * @throws MockedNetworkException if the mocked network response with an error.
	 */
	@PostMapping("/authorize")
	NetworkResponse doAuthorization(final NetworkAuthorizationRequest mockedNetworkRequest)
			throws MockedNetworkException;

	/**
	 * Consumes mocked network endpoint to process a capture request.
	 *
	 * @param captureMockedNetworkRequest Capture request to make to the network.
	 * @return Mocked network response.
	 * @throws MockedNetworkException if the mocked network response with an error.
	 */
	@PostMapping("/capture")
	NetworkResponse doCapture(final NetworkCaptureRequest captureMockedNetworkRequest) throws MockedNetworkException;

	/**
	 * Consumes mocked network endpoint to process a void request.
	 *
	 * @param voidMockedNetworkRequest Void request to make to the network.
	 * @return Mocked network response.
	 * @throws MockedNetworkException if the mocked network response with an error.
	 */
	@PostMapping("/void")
	NetworkResponse doVoid(final NetworkVoidRequest voidMockedNetworkRequest) throws MockedNetworkException;

	/**
	 * Consumes mocked network endpoint to process a refund request.
	 *
	 * @param refundMockedNetworkRequest Refund request to make to the network.
	 * @return Mocked network response.
	 * @throws MockedNetworkException if the mocked network response with an error.
	 */
	@PostMapping("/refund")
	NetworkResponse doRefund(final NetworkRefundRequest refundMockedNetworkRequest) throws MockedNetworkException;
}
