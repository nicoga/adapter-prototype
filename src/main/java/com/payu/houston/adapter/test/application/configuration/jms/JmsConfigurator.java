/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.component.jms.JmsConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

/**
 * Java Message Service configurator.
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "jms.config")
public class JmsConfigurator {

	/**
	 * Set to {@code true}, if you want to send message using the QoS settings specified on the message, instead of the
	 * QoS settings on the JMS endpoint. The following three headers are considered JMSPriority, JMSDeliveryMode, and
	 * JMSExpiration. You can provide all or only some of them. If not provided, Camel will fall back to use the values
	 * from the endpoint instead. So, when using this option, the headers override the values from the endpoint. The
	 * explicitQosEnabled option, by contrast, will only use options set on the endpoint, and not values from the
	 * message header.
	 */
	private boolean preserveMessageQos;

	/**
	 * Specifies the default number of concurrent consumers. This option can also be used when doing request/reply
	 * over JMS. See also the {@code maxMessagesPerTask} option to control dynamic scaling up/down of threads.
	 */
	private int concurrentConsumers;

	/**
	 * Specifies the maximum number of concurrent consumers. This option can also be used when doing request/reply
	 * over JMS. See also the {@code maxMessagesPerTask} option to control dynamic scaling up/down of threads. The
	 * {@code maxMessagesPerTask} option MUST be set to an integer greater than 0 for threads to scale down. Otherwise,
	 * the number of threads will remain at {@code maxConcurrentConsumers} until shutdown.
	 */
	private int maxConcurrentConsumers;

	/**
	 * The number of messages per task. The default, -1, is unlimited. If you use a range for concurrent consumers e.g.,
	 * concurrentConsumers < maxConcurrentConsumers, then this option can be used to set a value to e.g., 100 to control
	 * how fast the consumers will shrink when less work is required.
	 */
	private int maxMessagesPerTask;

	/**
	 * If enabled then the JmsConsumer may pickup the next message from the JMS queue, while the previous message is
	 * being processed asynchronously. This means that messages may be processed not 100% strictly in order. If
	 * disabled (as default) then the Exchange is fully processed before the JmsConsumer will pickup the next message
	 * from the JMS queue. Note if transacted has been enabled, then asyncConsumer=true does not run asynchronously,
	 * as transactions must be executed synchronously.
	 */
	private boolean asyncConsumer;

	/**
	 * If true, a producer will behave like a InOnly exchange with the exception that JMSReplyTo header is sent out
	 * and not be suppressed like in the case of InOnly. Like InOnly the producer will not wait for a reply. A
	 * consumer with this flag will behave like InOnly. This feature can be used to bridge InOut requests to another
	 * queue so that a route on the other queue will send it´s response directly back to the original JMSReplyTo.
	 */
	private boolean disableReplyTo;

	/**
	 * Specifies whether persistent delivery is used by default.
	 */
	private boolean deliveryPersistent;

	/**
	 * When sending messages, specifies the time-to-live of the message (in milliseconds).
	 */
	private int timeToLive;

	/**
	 * Should a JMS message be copied to a new JMS Message object as part of the send() method in JMS. This is
	 * enabled by default to be compliant with the JMS specification. You can disable it if you do not mutate JMS
	 * messages after they are sent for a performance boost.
	 * <p>
	 * Every Camel route will make a copy of the original incoming message before any modifications to it. This
	 * pristine copy of the message is kept in case it is needed to be redelivered during error handling or with
	 * onCompletion construct.
	 */
	private boolean copyMessageOnSend;

	/**
	 * Enables the use of compression of the message bodies.
	 * <p>
	 * When compression is enabled, the body of each JMS message (but not the headers) is compressed before it is
	 * sent across the wire. This results in smaller messages and better network performance. On the other hand, it
	 * has the disadvantage of being CPU intensive.
	 */
	private boolean useCompression;

	/**
	 * Default value for ActiveMQ must be 1000.
	 */
	private int maxThreadPoolSize;

	/**
	 * Enable priority support, so high priority messages are consumed before low priority.
	 */
	private boolean messagePrioritySupported;

	/**
	 * Enables or disables the default setting of whether or not consumers have their messages
	 * <a href="http://activemq.apache.org/consumer-dispatch-async.html">dispatched synchronously or asynchronously
	 * by the broker</a>. For non-durable topics for example we typically dispatch synchronously by default to
	 * minimize context switches which boost performance. However sometimes its better to go slower to ensure that a
	 * single blocked consumer socket does not block delivery to other consumers.
	 * <p>
	 * If you are sure that your consumers are always fast, you could achieve better performance by disabling
	 * asynchronous dispatch on the broker (thereby avoiding the cost of unnecessary context switching).
	 */
	private boolean dispatchAsync;

	/**
	 * ActiveMQ supports sending messages to a broker in either synchronous or asynchronous mode. The selected mode has
	 * a large impact on the latency of the send call: synchronous mode increases latency and can lead to a reduction in
	 * the producer's throughput; asynchronous mode generally improves throughput, but it also affects reliability.
	 * <p>
	 * Only enable the use of async sends to increase throughput if the acquirer was designed to tolerate a small
	 * amount of message loss in failure scenarios.
	 */
	private boolean useAsyncSend;

	/**
	 * Enable statistics recollection
	 */
	private boolean statsEnabled;

	/**
	 * Creates the Connection Factory to use during JMS messages transmission.
	 *
	 * @param brokerConfigurator
	 * @return A new Pooled Connection Factory with an embedded ActiveMQ Connection Factory
	 */
	@Bean("jmsConnectionFactory")
	public PooledConnectionFactory getConnectionFactory(final BrokerConfigurator brokerConfigurator) {

		final ActiveMQConnectionFactory jmsConnectionFactory = brokerConfigurator.buildSslConnectionFactory();
		jmsConnectionFactory.setCopyMessageOnSend(isCopyMessageOnSend());
		jmsConnectionFactory.setUseCompression(isUseCompression());
		jmsConnectionFactory.setDispatchAsync(isDispatchAsync());
		jmsConnectionFactory.setMessagePrioritySupported(isMessagePrioritySupported());
		jmsConnectionFactory.setMaxThreadPoolSize(getMaxThreadPoolSize());
		jmsConnectionFactory.setUseAsyncSend(isUseAsyncSend());
		jmsConnectionFactory.setStatsEnabled(isStatsEnabled());
		jmsConnectionFactory.setWatchTopicAdvisories(false);

		final PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
		pooledConnectionFactory.setConnectionFactory(jmsConnectionFactory);

		return pooledConnectionFactory;
	}

	/**
	 * Creates a JMS component that allows messages to be sent to (or consumed from) a JMS Queue
	 *
	 * @param connectionFactory connection factory to use
	 * @return A new ActiveMQ JMS Component Bean
	 */
	@Bean("activemq")
	public JmsComponent getJmsComponent(final ConnectionFactory connectionFactory) {

		final JmsConfiguration jmsConfig = new JmsConfiguration();
		jmsConfig.setConnectionFactory(connectionFactory);
		jmsConfig.setPreserveMessageQos(isPreserveMessageQos());
		jmsConfig.setConcurrentConsumers(getConcurrentConsumers());
		jmsConfig.setMaxConcurrentConsumers(getMaxConcurrentConsumers());
		jmsConfig.setMaxMessagesPerTask(getMaxMessagesPerTask());
		jmsConfig.setAsyncConsumer(isAsyncConsumer());
		jmsConfig.setDisableReplyTo(isDisableReplyTo());
		jmsConfig.setDeliveryPersistent(isDeliveryPersistent());
		jmsConfig.setAllowNullBody(false);
		jmsConfig.setTimeToLive(getTimeToLive());

		final JmsComponent comp = new ActiveMQComponent();
		comp.setConfiguration(jmsConfig);
		return comp;

	}

	/**
	 * Returns the {@code preserveMessageQos} JMS configuration variable
	 *
	 * @return The {@code preserveMessageQos} JMS configuration variable
	 * @see #preserveMessageQos
	 */
	public boolean isPreserveMessageQos() {

		return preserveMessageQos;
	}

	/**
	 * Sets the value for the {@code preserveMessageQos} JMS configuration variable
	 *
	 * @param preserveMessageQos The value to set
	 * @see #preserveMessageQos
	 */
	public void setPreserveMessageQos(boolean preserveMessageQos) {

		this.preserveMessageQos = preserveMessageQos;
	}

	/**
	 * Returns the {@code concurrentConsumers} JMS configuration variable
	 *
	 * @return The {@code concurrentConsumers} JMS configuration variable
	 * @see #concurrentConsumers
	 */
	public int getConcurrentConsumers() {

		return concurrentConsumers;
	}

	/**
	 * Sets the value for the {@code concurrentConsumers} JMS configuration variable
	 *
	 * @param concurrentConsumers The value to set
	 * @see #concurrentConsumers
	 */
	public void setConcurrentConsumers(int concurrentConsumers) {

		this.concurrentConsumers = concurrentConsumers;
	}

	/**
	 * Returns the {@code maxConcurrentConsumers} JMS configuration variable
	 *
	 * @return The {@code maxConcurrentConsumers} JMS configuration variable
	 * @see #maxConcurrentConsumers
	 */
	public int getMaxConcurrentConsumers() {

		return maxConcurrentConsumers;
	}

	/**
	 * Sets the value for the {@code maxConcurrentConsumers} JMS configuration variable
	 *
	 * @param maxConcurrentConsumers The value to set
	 * @see #maxConcurrentConsumers
	 */
	public void setMaxConcurrentConsumers(int maxConcurrentConsumers) {

		this.maxConcurrentConsumers = maxConcurrentConsumers;
	}

	/**
	 * Returns the {@code maxMessagesPerTask} JMS configuration variable
	 *
	 * @return The {@code maxMessagesPerTask} JMS configuration variable
	 * @see #maxMessagesPerTask
	 */
	public int getMaxMessagesPerTask() {

		return maxMessagesPerTask;
	}

	/**
	 * Sets the value for the {@code maxMessagesPerTask} JMS configuration variable
	 *
	 * @param maxMessagesPerTask The value to set
	 * @see #maxMessagesPerTask
	 */
	public void setMaxMessagesPerTask(int maxMessagesPerTask) {

		this.maxMessagesPerTask = maxMessagesPerTask;
	}

	/**
	 * Returns the {@code asyncConsumer} JMS configuration variable
	 *
	 * @return The {@code asyncConsumer} JMS configuration variable
	 * @see #asyncConsumer
	 */
	public boolean isAsyncConsumer() {

		return asyncConsumer;
	}

	/**
	 * Sets the value for the {@code asyncConsumer} JMS configuration variable
	 *
	 * @param asyncConsumer The value to set
	 * @see #asyncConsumer
	 */
	public void setAsyncConsumer(boolean asyncConsumer) {

		this.asyncConsumer = asyncConsumer;
	}

	/**
	 * Returns the {@code disableReplyTo} JMS configuration variable
	 *
	 * @return The {@code disableReplyTo} JMS configuration variable
	 * @see #disableReplyTo
	 */
	public boolean isDisableReplyTo() {

		return disableReplyTo;
	}

	/**
	 * Sets the value for the {@code disableReplyTo} JMS configuration variable
	 *
	 * @param disableReplyTo The value to set
	 * @see #disableReplyTo
	 */
	public void setDisableReplyTo(boolean disableReplyTo) {

		this.disableReplyTo = disableReplyTo;
	}

	/**
	 * Returns the {@code deliveryPersistent} JMS configuration variable
	 *
	 * @return The {@code deliveryPersistent} JMS configuration variable
	 * @see #deliveryPersistent
	 */
	public boolean isDeliveryPersistent() {

		return deliveryPersistent;
	}

	/**
	 * Sets the value for the {@code deliveryPersistent} JMS configuration variable
	 *
	 * @param deliveryPersistent The value to set
	 * @see #deliveryPersistent
	 */
	public void setDeliveryPersistent(boolean deliveryPersistent) {

		this.deliveryPersistent = deliveryPersistent;
	}

	/**
	 * Returns the {@code timeToLive} JMS configuration variable
	 *
	 * @return The {@code timeToLive} JMS configuration variable
	 * @see #timeToLive
	 */
	public int getTimeToLive() {

		return timeToLive;
	}

	/**
	 * Sets the value for the {@code timeToLive} JMS configuration variable
	 *
	 * @param timeToLive The value to set
	 * @see #timeToLive
	 */
	public void setTimeToLive(int timeToLive) {

		this.timeToLive = timeToLive;
	}

	/**
	 * Returns the {@code copyMessageOnSend} JMS configuration variable
	 *
	 * @return The {@code copyMessageOnSend} JMS configuration variable
	 * @see #copyMessageOnSend
	 */
	public boolean isCopyMessageOnSend() {

		return copyMessageOnSend;
	}

	/**
	 * Sets the value for the {@code copyMessageOnSend} JMS configuration variable
	 *
	 * @param copyMessageOnSend The value to set
	 * @see #copyMessageOnSend
	 */
	public void setCopyMessageOnSend(boolean copyMessageOnSend) {

		this.copyMessageOnSend = copyMessageOnSend;
	}

	/**
	 * Returns the {@code useCompression} JMS configuration variable
	 *
	 * @return The {@code useCompression} JMS configuration variable
	 * @see #useCompression
	 */
	public boolean isUseCompression() {

		return useCompression;
	}

	/**
	 * Sets the value for the {@code useCompression} JMS configuration variable
	 *
	 * @param useCompression The value to set
	 * @see #useCompression
	 */
	public void setUseCompression(boolean useCompression) {

		this.useCompression = useCompression;
	}

	/**
	 * Returns the {@code maxThreadPoolSize} JMS configuration variable
	 *
	 * @return The {@code maxThreadPoolSize} JMS configuration variable
	 * @see #maxThreadPoolSize
	 */
	public int getMaxThreadPoolSize() {

		return maxThreadPoolSize;
	}

	/**
	 * Sets the value for the {@code maxThreadPoolSize} JMS configuration variable
	 *
	 * @param maxThreadPoolSize The value to set
	 * @see #maxThreadPoolSize
	 */
	public void setMaxThreadPoolSize(int maxThreadPoolSize) {

		this.maxThreadPoolSize = maxThreadPoolSize;
	}

	/**
	 * Returns the {@code messagePrioritySupported} JMS configuration variable
	 *
	 * @return The {@code messagePrioritySupported} JMS configuration variable
	 * @see #messagePrioritySupported
	 */
	public boolean isMessagePrioritySupported() {

		return messagePrioritySupported;
	}

	/**
	 * Sets the value for the {@code messagePrioritySupported} JMS configuration variable
	 *
	 * @param messagePrioritySupported The value to set
	 * @see #messagePrioritySupported
	 */
	public void setMessagePrioritySupported(boolean messagePrioritySupported) {

		this.messagePrioritySupported = messagePrioritySupported;
	}

	/**
	 * Returns the {@code dispatchAsync} JMS configuration variable
	 *
	 * @return The {@code dispatchAsync} JMS configuration variable
	 * @see #dispatchAsync
	 */
	public boolean isDispatchAsync() {

		return dispatchAsync;
	}

	/**
	 * Sets the value for the {@code dispatchAsync} JMS configuration variable
	 *
	 * @param dispatchAsync The value to set
	 * @see #dispatchAsync
	 */
	public void setDispatchAsync(boolean dispatchAsync) {

		this.dispatchAsync = dispatchAsync;
	}

	/**
	 * Returns the {@code useAsyncSend} JMS configuration variable
	 *
	 * @return The {@code useAsyncSend} JMS configuration variable
	 * @see #useAsyncSend
	 */
	public boolean isUseAsyncSend() {

		return useAsyncSend;
	}

	/**
	 * Sets the value for the {@code useAsyncSend} JMS configuration variable
	 *
	 * @param useAsyncSend The value to set
	 * @see #useAsyncSend
	 */
	public void setUseAsyncSend(boolean useAsyncSend) {

		this.useAsyncSend = useAsyncSend;
	}

	/**
	 * Returns the {@code statsEnabled} JMS configuration variable
	 *
	 * @return The {@code statsEnabled} JMS configuration variable
	 * @see #statsEnabled
	 */
	public boolean isStatsEnabled() {

		return statsEnabled;
	}

	/**
	 * Sets the value for the {@code statsEnabled} JMS configuration variable
	 *
	 * @param statsEnabled The value to set
	 * @see #statsEnabled
	 */
	public void setStatsEnabled(boolean statsEnabled) {

		this.statsEnabled = statsEnabled;
	}

}
