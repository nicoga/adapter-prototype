/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.service;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.test.model.CardHolder;
import com.payu.houston.adapter.test.model.CreditCard;
import com.payu.houston.adapter.test.model.Merchant;
import com.payu.houston.adapter.test.model.Order;
import com.payu.houston.adapter.test.model.constant.ParametersName;
import com.payu.houston.adapter.test.model.constant.TransactionTypes;
import com.payu.houston.adapter.test.model.request.NetworkAuthorizationRequest;
import com.payu.houston.adapter.test.model.request.NetworkCaptureRequest;
import com.payu.houston.adapter.test.model.request.NetworkRefundRequest;
import com.payu.houston.adapter.test.model.request.NetworkVoidRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Creates requests in order to process transactions int the mocked network.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Component
public class MockedNetworkRequestFactory {

	/**
	 * Api key for authentication in the mocked network
	 */
	private static String API_KEY;

	/**
	 * Api login for authentication in the mocked network.
	 */
	private static String API_LOGIN;

	/**
	 * Crates an authorization request for the mocked network.
	 *
	 * @param requestMessage Request message of which is going to be extracted request info.
	 * @return Authorization request.
	 */
	public static NetworkAuthorizationRequest createAuthorizationRequest(
			final AuthorizationRequestMessage requestMessage) {

		return NetworkAuthorizationRequest.builder()
				.cardHolder(getCardHolderData(requestMessage.getPaymentInstrument()))
				.creditCard(getCreditCardData(requestMessage.getPaymentInstrument()))
				.merchant(Merchant.builder().apiKey(API_KEY).apiLogin(API_LOGIN).build())
				.order(getOrderData(requestMessage)).type(TransactionTypes.AUTHORIZATION.name()).build();
	}

	/**
	 * Gets order information from the request message.
	 *
	 * @param requestMessage PRequest message of which is going to be extracted the order information.
	 * @return Order information.
	 */
	private static Order getOrderData(final AuthorizationRequestMessage requestMessage) {

		return Order.builder().amount(requestMessage.getAmount().getTotal().toString())
				.currency(requestMessage.getAmount().getCurrency())
				.installments(
						Integer.parseInt(requestMessage.getPaymentInstrument().get(ParametersName.INSTALLMENTS.name())))
				.orderId(requestMessage.getOrder().getId().toString()).build();
	}

	/**
	 * Gets the credit card information from the payment instrument.
	 *
	 * @param paymentInstrument Payment instrument of which is going to be extracted the credit card  information.
	 * @return Credit card information.
	 */
	private static CreditCard getCreditCardData(final Map<String, String> paymentInstrument) {

		return CreditCard.builder().cardNumber(paymentInstrument.get(ParametersName.CARD_NUMBER.name()))
				.cvv(paymentInstrument.get(ParametersName.CARD_SECURITY_CODE.name()))
				.expirationMonth(paymentInstrument.get(ParametersName.CARD_EXPIRATION_DATE.name()).substring(0, 2))
				.expirationYear(paymentInstrument.get(ParametersName.CARD_EXPIRATION_DATE.name()).substring(3, 7))
				.build();
	}

	/**
	 * Gets the card holder information from the payment instrument.
	 *
	 * @param paymentInstrument Payment instrument of which is going to be extracted the card holder information.
	 * @return Card holder information.
	 */
	private static CardHolder getCardHolderData(final Map<String, String> paymentInstrument) {

		return CardHolder.builder()
				.documentNumber(paymentInstrument.get(ParametersName.PAYER_IDENTITY_DOC_NUMBER.name()))
				.documentType(paymentInstrument.get(ParametersName.PAYER_IDENTITY_DOC_TYPE.name()))
				.email(paymentInstrument.get(ParametersName.PAYER_EMAIL.name()))
				.fullName(paymentInstrument.get(ParametersName.CARDHOLDER_NAME.name()))
				.phoneNumber(paymentInstrument.get(ParametersName.PAYER_PHONE.name())).build();
	}

	/**
	 * Creates the capture request for the mocked network.
	 *
	 * @param request Request message of which is going to be extracted request info.
	 * @return Mocked network capture request.
	 */
	public static NetworkCaptureRequest createCaptureRequest(final CaptureRequestMessage request) {

		return NetworkCaptureRequest.builder().captureAmount(request.getAmount().getTotal().toString()).merchant(
				Merchant.builder().apiLogin(API_LOGIN).apiKey(API_KEY).build())
				.orderId(request.getAuthorizationTraceabilityId()).build();
	}

	/**
	 * Creates the void request for the mocked network.
	 *
	 * @param request Request message of which is going to be extracted request info.
	 * @return Mocked network void request.
	 */
	public static NetworkVoidRequest createVoidRequest(final VoidRequestMessage request) {

		return NetworkVoidRequest.builder().merchant(
				Merchant.builder().apiLogin(API_LOGIN).apiKey(API_KEY).build())
				.orderId(request.getAuthorizationTraceabilityId()).build();
	}

	/**
	 * Creates the refund request for the mocked network.
	 *
	 * @param request Request message of which is going to be extracted request info.
	 * @return Mocked network refund request.
	 */
	public static NetworkRefundRequest createRefundRequest(final RefundRequestMessage request) {

		return NetworkRefundRequest.builder().merchant(
				Merchant.builder().apiLogin(API_LOGIN).apiKey(API_KEY).build())
				.orderId(request.getCaptureTraceabilityId()).refundAmount(request.getAmount().getTotal().toString())
				.reason("Merchant requested").build();
	}

	@Value("${mockedNetwork.apiKey}")
	public void setMerchantApiKey(final String merchantApiKey) {

		MockedNetworkRequestFactory.API_KEY = merchantApiKey;
	}

	@Value("${mockedNetwork.apiLogin}")
	public void setMerchantApiLogin(final String merchantApiLogin) {

		MockedNetworkRequestFactory.API_LOGIN = merchantApiLogin;
	}
}
