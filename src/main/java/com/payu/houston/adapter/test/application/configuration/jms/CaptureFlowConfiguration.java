/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.test.application.GenericFlow;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Configuration class that enables the message flow for capture request.
 *
 * @author Santiago Alzate S. (santiago.alzate@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Component
@ConfigurationProperties(prefix = "test.capture")
public class CaptureFlowConfiguration extends GenericFlow {

	/**
	 * The name of the method on the provider component that handles the operation request message and produces the
	 * response message.
	 */
	public static final String PROVIDER_HANDLER_METHOD = "doCapture";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProviderHandlerMethod() {

		return PROVIDER_HANDLER_METHOD;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Class<CaptureRequestMessage> getRequestMessageClass() {

		return CaptureRequestMessage.class;
	}
}
