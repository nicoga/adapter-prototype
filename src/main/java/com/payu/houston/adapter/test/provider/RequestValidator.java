/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.provider;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.test.model.constant.ParametersName;
import org.apache.commons.lang3.StringUtils;

import static java.util.Objects.nonNull;

/**
 * Validate transaction requests.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class RequestValidator {

	/**
	 * Validates  an authorization request.
	 *
	 * @param request Request to validate.
	 * @return If the request is complete.
	 */
	public static boolean validateAuthorizationRequestMessage(final AuthorizationRequestMessage request) {

		return nonNull(request.getPaymentInstrument()) && nonNull(request.getAmount()) && nonNull(request.getOrder())
				&& nonNull(request.getPaymentInstrument().get(ParametersName.CARD_EXPIRATION_DATE.name())) && nonNull(
				request.getPaymentInstrument().get(ParametersName.CARDHOLDER_NAME.name())) && nonNull(
				request.getPaymentInstrument().get(ParametersName.INSTALLMENTS.name())) && nonNull(
				request.getPaymentInstrument().get(ParametersName.PAYER_EMAIL.name())) && nonNull(
				request.getPaymentInstrument().get(ParametersName.PAYER_IDENTITY_DOC_NUMBER.name())) && nonNull(
				request.getPaymentInstrument().get(ParametersName.PAYER_IDENTITY_DOC_TYPE.name())) && nonNull(
				request.getPaymentInstrument().get(ParametersName.PAYER_PHONE.name())) && StringUtils
				.isNumeric(request.getPaymentInstrument().get(ParametersName.CARD_NUMBER.name())) && StringUtils
				.isNumeric(request.getPaymentInstrument().get(ParametersName.INSTALLMENTS.name()));
	}

	/**
	 * Validates a capture request.
	 *
	 * @param request Capture request to validate.
	 * @return If the request is correct.
	 */
	public static boolean validateCaptureRequestMessage(final CaptureRequestMessage request) {

		return nonNull(request.getAuthorizationTraceabilityId()) && nonNull(request.getAmount().getTotal());
	}

	/**
	 * Validates a void request.
	 *
	 * @param request Void request to validate.
	 * @return If the request is correct.
	 */
	public static boolean validateVoidRequestMessage(final VoidRequestMessage request) {

		return nonNull(request.getAuthorizationTraceabilityId());
	}

	/**
	 * Validates a refund request.
	 *
	 * @param request Refund request to validate.
	 * @return If the request is correct.
	 */
	public static boolean validateRefundRequest(final RefundRequestMessage request) {

		return nonNull(request.getCaptureTraceabilityId()) && nonNull(request.getAmount()) && nonNull(request.getAmount().getTotal());
	}
}
