/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */

package com.payu.houston.adapter.test.provider;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseCode;
import com.payu.houston.adapter.queue.model.common.RequestMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Utilities for Test Adapter
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public final class HoustonTestProviderHelper {

	/**
	 * The class logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(HoustonTestProviderHelper.class);

	/**
	 * The character to identify a delayed transaction in security code
	 **/
	private static final char INITIAL_CHAR_TO_DELAYED = '5';

	/**
	 * Private constructor to avoid direct class instantiation (Utility Class)
	 */
	private HoustonTestProviderHelper() {

	}

	/**
	 * Adds a delay based on credit card security code
	 *
	 * @param request The request message
	 */
	public static void addDelay(final RequestMessage request) {

		try {

			if (request.getPaymentInstrument() != null) {
				final String securityCode = request.getPaymentInstrument().get("CARD_SECURITY_CODE");
				if (securityCode != null && securityCode.charAt(0) == INITIAL_CHAR_TO_DELAYED) {
					final String seconds = securityCode.substring(1, securityCode.length());
					LOGGER.trace("Add delay of  [{}] seconds to request. CorrelationId: [{}]",
							seconds, request.getCorrelationId());
					TimeUnit.SECONDS.sleep(Long.valueOf(seconds));
				}
			}

		} catch (NumberFormatException | InterruptedException e) {
			LOGGER.warn("Couldn't add the desired delay. Error message [{}]", e.getMessage());
		}
	}

	/**
	 * Determines the appropriate Authorization response code depending on the information of the request (Name of the
	 * payer or cardholder)
	 *
	 * @param name Cardholder or payer's name
	 * @return The calculated Authorization response code
	 */
	public static AuthorizationResponseCode getAuthResponseCode(final String name) {

		final AuthorizationResponseCode responseCode;

		if (name != null) {
			switch (name) {
			case "NETWORK_DECLINED":
				responseCode = AuthorizationResponseCode.PAYMENT_NETWORK_REJECTED;
				break;

			case "ENTITY_DECLINED":
				responseCode = AuthorizationResponseCode.ENTITY_DECLINED;
				break;

			case "NETWORK_BAD_RESPONSE":
				responseCode = AuthorizationResponseCode.PAYMENT_NETWORK_BAD_RESPONSE;
				break;

			case "NETWORK_NO_RESPONSE":
				responseCode = AuthorizationResponseCode.PAYMENT_NETWORK_NO_RESPONSE;
				break;

			case "NETWORK_ERROR":
			case "ERROR":
				responseCode = AuthorizationResponseCode.ERROR;
				break;

			case "NETWORK_APPROVED":
			case "ENTITY_APPROVED":
			default:
				responseCode = AuthorizationResponseCode.APPROVED;
				break;
			}

		} else {
			responseCode = AuthorizationResponseCode.APPROVED;
		}

		return responseCode;

	}
}
