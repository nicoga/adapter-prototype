/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/24/2020
 */
package com.payu.houston.adapter.test.model;

import lombok.Builder;
import lombok.Data;

/**
 * Mocked network transaction response definition.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
public class NetworkResponse {

	/**
	 * Details of the error occurred while attempting the transaction.
	 */
	private final String errorDetails;

	/**
	 * Transaction code.
	 */
	private final String transactionCode;

	/**
	 * Transaction date.
	 */
	private final String transactionDate;

	/**
	 * Order id.
	 */
	private final String orderId;

	/**
	 * Transaction response code.
	 */
	private final String responseCode;

	/**
	 * Transaction response message.
	 */
	private final String responseMessage;

	/**
	 * Authorized amount.
	 */
	private final String amount;

	/**
	 * State of the transaction.
	 */
	private final String state;

}
