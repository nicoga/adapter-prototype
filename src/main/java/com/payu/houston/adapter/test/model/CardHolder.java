/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/20/2020
 */
package com.payu.houston.adapter.test.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Size;

/**
 * Mocked network card holder definition.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
public class CardHolder {

	/**
	 * Full name.
	 */
	@Size(min = 1, max = 75)
	private String fullName;

	/**
	 * Card holder email.
	 */
	@Size(min = 1, max = 35)
	private String email;

	/**
	 * Phone number
	 */
	@Size(min = 1, max = 10)
	private String phoneNumber;

	/**
	 * Dni type.
	 */
	private String documentType;

	/**
	 * Dni number.
	 */
	@Size(min = 8, max = 12)
	private String documentNumber;
}
