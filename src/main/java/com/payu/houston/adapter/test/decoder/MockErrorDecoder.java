/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.decoder;

import com.payu.houston.adapter.test.exception.MockedNetworkException;
import com.payu.houston.adapter.test.model.NetworkResponse;
import feign.FeignException;
import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.isNull;

/**
 * Mock error decoder in case of error response code of mocked network.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class MockErrorDecoder implements ErrorDecoder {

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MockErrorDecoder.class);

	/**
	 * Variable for format error in case mocked network  error.
	 */
	private static final String ERROR_FORMAT = "Error %d received while calling mocked network %s method";

	/**
	 * Method for mapping the mocked network response to  an exception.
	 *
	 * @param message  message error returned.
	 * @param response model generic of feign.
	 * @return Correct exception
	 */
	@Override
	public Exception decode(final String message, final Response response) {

		String errorMessage = String.format(ERROR_FORMAT, response.status(), message);

		if (isNull(response.body())) {
			return FeignException.errorStatus(message, response);
		}

		try {
			Decoder.Default decoder = new Decoder.Default();
			String body = (String) decoder.decode(response, String.class);
			errorMessage += ", with response body " + body;
			NetworkResponse networkResponse = (NetworkResponse) decoder.decode(response, NetworkResponse.class);
			return new MockedNetworkException(errorMessage, networkResponse);
		} catch (Exception e) {
			LOGGER.warn("Unexpected error decoding the error message", e);
		}

		return FeignException.errorStatus(errorMessage, response);
	}
}
