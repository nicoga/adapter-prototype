/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration.jms;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSslConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Activemq Broker configurator.
 *
 * @author David Hidalgo (david.hidalgo@payulatam.com) 
 * @version 1.0
 * @since 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "spring.activemq")
public class BrokerConfigurator {

	/**
	 * Class logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerConfigurator.class);

	/**
	 * The ActiveMQ broker URL to connect to.
	 */
	private String brokerUrl;

	/**
	 * The ActiveMQ broker trustStore.
	 */
	private String trustStore;

	/**
	 * The password required to access the trustStore of the ActiveMQ broker
	 */
	private String trustStorePassword;

	/**
	 * The ActiveMQ broker keyStore
	 */
	private String keyStore;

	/**
	 * The password required to access the KeyStore of the ActiveMQ broker
	 */
	private String keyStorePassword;

	/**
	 * Configures a Connection Factory that will be used to manage the connections to the ActiveMQ broker. If one of
	 * the KeyStore or TrustStore properties is set, use it to create a secure connection using SSL.
	 *
	 * @return A ActiveMQConnectionFactory object
	 * @see ActiveMQConnectionFactory
	 * @see ActiveMQSslConnectionFactory
	 */
	public ActiveMQConnectionFactory buildSslConnectionFactory() {

		if (StringUtils.isNotBlank(trustStore) || StringUtils.isNotBlank(keyStore)) {

			final ActiveMQSslConnectionFactory connectionFactory = new ActiveMQSslConnectionFactory(brokerUrl);
			configureSslConnectionFactoryWithTrustStore(connectionFactory);
			configureSslConnectionFactoryWithKeyStore(connectionFactory);
			return connectionFactory;

		} else {
			return new ActiveMQConnectionFactory(brokerUrl);
		}
	}

	/**
	 * Configures a connection factory using the TrustStore information
	 *
	 * @return A new connection factory configured with the TrustStore path and password
	 */
	private void configureSslConnectionFactoryWithTrustStore(final ActiveMQSslConnectionFactory connectionFactory) {

		try {
			if(StringUtils.isNotBlank(trustStore)){
				LOGGER.info("Configuring ActiveMQ connection factory supporting SSL from TrustStore");
				connectionFactory.setTrustStore(trustStore);
				connectionFactory.setTrustStorePassword(trustStorePassword);
			}
		} catch (Exception e) {
			LOGGER.warn("Error setting TrustStore to connection factory", e);
		}
	}

	/**
	 * Configures a connection factory using the KeyStore information
	 *
	 * @return A new connection factory configured with the KeyStore path and password
	 */
	private void configureSslConnectionFactoryWithKeyStore(final ActiveMQSslConnectionFactory connectionFactory) {

		try {
			if(StringUtils.isNotBlank(keyStore)){
				LOGGER.info("Configuring ActiveMQ connection factory supporting SSL from KeyStore");
				connectionFactory.setKeyStore(keyStore);
				connectionFactory.setKeyStorePassword(keyStorePassword);
			}
		} catch (Exception e) {
			LOGGER.warn("Error setting KeyStore to connection factory", e);
		}
	}

	/**
	 * Returns the broker URL
	 *
	 * @return the brokerURL
	 */
	public String getBrokerUrl() {

		return brokerUrl;
	}

	/**
	 * Returns the path to the trustStore
	 *
	 * @return the trustStore
	 */
	public String getTrustStore() {

		return trustStore;
	}

	/**
	 * Returns the password of the TrustStore
	 *
	 * @return the trustStorePassword
	 */
	public String getTrustStorePassword() {

		return trustStorePassword;
	}

	/**
	 * Returns the path of the KeyStore
	 *
	 * @return the keyStore
	 */
	public String getKeyStore() {

		return keyStore;
	}

	/**
	 * Returns the password of the KeyStore
	 *
	 * @return the keyStorePassword
	 */
	public String getKeyStorePassword() {

		return keyStorePassword;
	}

	/**
	 * @param brokerURL the brokerURL to set
	 */
	public void setBrokerUrl(String brokerURL) {

		this.brokerUrl = brokerURL;
	}

	/**
	 * @param trustStore the trustStore to set
	 */
	public void setTrustStore(String trustStore) {

		this.trustStore = trustStore;
	}

	/**
	 * @param trustStorePassword the trustStorePassword to set
	 */
	public void setTrustStorePassword(String trustStorePassword) {

		this.trustStorePassword = trustStorePassword;
	}

	/**
	 * @param keyStore the keystore to set
	 */
	public void setKeyStore(String keyStore) {

		this.keyStore = keyStore;
	}

	/**
	 * @param keyStorePassword the keyStorePassword to set
	 */
	public void setKeyStorePassword(String keyStorePassword) {

		this.keyStorePassword = keyStorePassword;
	}

}
