/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/20/2020
 */
package com.payu.houston.adapter.test.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Size;

/**
 * Mocked network credit card definition.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
public class CreditCard {

	/**
	 * Card number.
	 */
	@Size(min = 14, max = 16)
	private String cardNumber;

	/**
	 * Card expiration month.
	 */
	private String expirationMonth;

	/**
	 * Card expiration year
	 */
	private String expirationYear;

	/**
	 * Card cvv.
	 */
	@Size(min = 3, max = 4)
	private String cvv;
}
