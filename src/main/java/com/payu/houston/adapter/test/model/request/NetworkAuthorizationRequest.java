/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.model.request;

import com.payu.houston.adapter.test.model.CardHolder;
import com.payu.houston.adapter.test.model.CreditCard;
import com.payu.houston.adapter.test.model.Merchant;
import com.payu.houston.adapter.test.model.Order;
import lombok.Builder;
import lombok.Data;

/**
 * Mocked network transaction request definition.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
public class NetworkAuthorizationRequest {

	/**
	 * Type of transaction.
	 */
	private String type;

	/**
	 * Merchant info.
	 */
	private Merchant merchant;

	/**
	 * Credit card details.
	 */
	private CreditCard creditCard;

	/**
	 * Card holder details.
	 */
	private CardHolder cardHolder;

	/**
	 * Order details.
	 */
	private Order order;

}
