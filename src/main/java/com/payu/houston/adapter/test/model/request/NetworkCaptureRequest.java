/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.model.request;

import com.payu.houston.adapter.test.model.Merchant;
import lombok.Builder;
import lombok.Data;

/**
 * Mocked network capture request definition.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
public class NetworkCaptureRequest {

	/**
	 * Merchant login details.
	 */
	private Merchant merchant;

	/**
	 * Order identification number.
	 */
	private String orderId;

	/**
	 * Amount to capture.
	 */
	private String captureAmount;
}
