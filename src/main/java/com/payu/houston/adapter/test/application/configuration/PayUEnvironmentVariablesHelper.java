/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/11/2020
 */
package com.payu.houston.adapter.test.application.configuration;

/**
 * OS environment variables helper
 *
 * @author Manuel E. Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public class PayUEnvironmentVariablesHelper {

	/**
	 * The name of the environment variable that stores the SALT used by the Cryptographic algorithm
	 */
	public static final String PAYU_SAL = "POL_SAL";

	/**
	 * The name of the environment variable that stores the password/key used by the Cryptographic algorithm
	 */
	public static final String PAYU_KEY = "POL_KEY";

	/**
	 * The name of the environment variable that stores the number of iterations that the Cryptographic algorithm
	 * uses to transform values
	 */
	public static final String PAYU_ITER = "POL_ITERACIONES";

	/**
	 * SALT used by the cryptographic algorithm. The SALT is a 64bits length value added to the key at each iteration
	 */
	private final String payuSalt;

	/**
	 * Number or iterations that the cryptographic algorithm converts the value
	 */
	private final String payuIter;

	/**
	 * The KEY used by the cryptographic algorithm
	 */
	private final String payuKey;

	/**
	 * Constructor with the injected values
	 */
	public PayUEnvironmentVariablesHelper(final String payuSalt, final String payuIter, final String payuKey) {

		this.payuSalt = payuSalt != null ? payuSalt : System.getenv(PAYU_SAL);
		this.payuIter = payuIter != null ? payuIter : System.getenv(PAYU_ITER);
		this.payuKey = payuKey != null ? payuKey : System.getenv(PAYU_KEY);
	}

	/**
	 * Returns the payuKey
	 *
	 * @return the payuKey
	 */
	public String getPayuKey() {
		return payuKey;
	}

	/**
	 * Returns the payuIter
	 *
	 * @return the payuIter
	 */
	public String getPayuIter() {
		return payuIter;
	}

	/**
	 * Returns the payuSalt
	 *
	 * @return the payuSalt
	 */
	public String getPayuSalt() {
		return payuSalt;
	}
}
