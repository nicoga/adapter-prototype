/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.model.constant;

/**
 * Parameters  definition.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public enum ParametersName {

	/**
	 * Name of credit card expiration date  payment Instrument field.
	 */
	CARD_EXPIRATION_DATE,

	/**
	 * Credit card number payment instrument field.
	 */
	CARD_NUMBER,

	/**
	 * Name of credit card security code  paymentInstrument field.
	 */
	CARD_SECURITY_CODE,

	/**
	 * Name of payer email  paymentInstrument field.
	 */
	PAYER_EMAIL,

	/**
	 * Name of payer phone number  paymentInstrument field.
	 */
	PAYER_PHONE,

	/**
	 * Name of payer identity type  paymentInstrument field.
	 */
	PAYER_IDENTITY_DOC_TYPE,

	/**
	 * Name of payer identification number paymentInstrument field.
	 */
	PAYER_IDENTITY_DOC_NUMBER,

	/**
	 * Name of installments for the  paymentInstrument field.
	 */
	INSTALLMENTS,

	/**
	 * Name of credit card holder name field.
	 */
	CARDHOLDER_NAME,

	/**
	 * State of the transaction in the mocked network response.
	 */
	STATE,

	/**
	 * Transaction code in the mocked network response.
	 */
	TRANSACTION_CODE,

	/**
	 * Transaction date in the mocked network response.
	 */
	TRANSACTION_DATE,

	/**
	 * Order id in the mocked network response.
	 */
	ORDER_ID,

	/**
	 * Transaction response code in the mocked network response.
	 */
	RESPONSE_CODE,

	/**
	 * Transaction response message in the mocked network response.
	 */
	RESPONSE_MESSAGE,

	/**
	 * Authorized amount in the mocked network response.
	 */
	AMOUNT,

	/**
	 * Details of the error occurred while attempting the transaction in the mocked network response.
	 */
	ERROR_DETAILS

}
