/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/20/2020
 */
package com.payu.houston.adapter.test.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Mocked network order definition.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
public class Order {

	/**
	 * Total amount of the order.
	 */
	private String amount;

	/**
	 * Currency of the placed order.
	 */
	private String currency;

	/**
	 * Installments of the order.
	 */
	@Min(1) @Max(48)
	private int installments;

	/**
	 * External commerce id for the order
	 */
	private String orderId;
}
