/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.service.impl;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationRequestMessage;
import com.payu.houston.adapter.queue.model.capture.CaptureRequestMessage;
import com.payu.houston.adapter.queue.model.refunds.RefundRequestMessage;
import com.payu.houston.adapter.queue.model.voids.VoidRequestMessage;
import com.payu.houston.adapter.test.client.MockedNetworkClient;
import com.payu.houston.adapter.test.exception.MockedNetworkException;
import com.payu.houston.adapter.test.model.NetworkResponse;
import com.payu.houston.adapter.test.model.request.NetworkAuthorizationRequest;
import com.payu.houston.adapter.test.model.request.NetworkCaptureRequest;
import com.payu.houston.adapter.test.model.request.NetworkRefundRequest;
import com.payu.houston.adapter.test.model.request.NetworkVoidRequest;
import com.payu.houston.adapter.test.service.MockedNetworkRequestFactory;
import com.payu.houston.adapter.test.service.NetworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Service implementation of {@linkplain NetworkService}
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Service
public class NetworkServiceImpl implements NetworkService {

	private static final String ERROR_STATUS = "ERROR";

	/**
	 * Logger class.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(NetworkServiceImpl.class);

	/**
	 * Client that consumes the mocked network.
	 */
	private final MockedNetworkClient mockedNetworkClient;

	/**
	 * Service constructor.
	 *
	 * @param mockedNetworkClient Mocked network feign client.
	 */
	public NetworkServiceImpl(MockedNetworkClient mockedNetworkClient) {

		this.mockedNetworkClient = mockedNetworkClient;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override public NetworkResponse doAuthorization(final AuthorizationRequestMessage request) {

		final NetworkAuthorizationRequest mockedNetworkRequest = MockedNetworkRequestFactory
				.createAuthorizationRequest(request);

		try {
			final NetworkResponse networkResponse = mockedNetworkClient.doAuthorization(mockedNetworkRequest);
			LOGGER.info(
					"Authorization request with correlation id:[{}] successfully made to the mocked network with response:[{}}",
					request.getCorrelationId(),
					networkResponse);
			return networkResponse;
		} catch (MockedNetworkException ex) {
			LOGGER.info("Mocked network authorization  with correlation id:[{}] response with error:{}",
					request.getCorrelationId(), ex.getErrorResponse(), ex);
			return ex.getErrorResponse();
		} catch (RuntimeException ex) {
			LOGGER.warn("Mocked network authorization  with correlation id:[{}] error:[{}]", request.getCorrelationId(),
					ex.getMessage(), ex);
			return NetworkResponse.builder().state(ERROR_STATUS).errorDetails(ex.getMessage()).build();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override public NetworkResponse doCapture(final CaptureRequestMessage request) {

		final NetworkCaptureRequest captureMockedNetworkRequest = MockedNetworkRequestFactory
				.createCaptureRequest(request);

		try {
			final NetworkResponse networkResponse = mockedNetworkClient.doCapture(captureMockedNetworkRequest);
			LOGGER.info(
					"Capture request  with correlation id:[{}] successfully made to the mocked network with response:{}",
					request.getCorrelationId(),
					networkResponse);
			return networkResponse;
		} catch (MockedNetworkException ex) {
			LOGGER.warn("Mocked network capture  with correlation id:[{}] response with error:{}",
					request.getCorrelationId(), ex.getErrorResponse(), ex);
			return ex.getErrorResponse();
		} catch (RuntimeException ex) {
			LOGGER.warn("Mocked network capture  with correlation id:[{}] error:[{}]", request.getCorrelationId(),
					ex.getMessage(), ex);
			return NetworkResponse.builder().state(ERROR_STATUS).errorDetails(ex.getMessage()).build();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override public NetworkResponse doVoid(final VoidRequestMessage request) {

		final NetworkVoidRequest voidMockedNetworkRequest = MockedNetworkRequestFactory.createVoidRequest(request);

		try {
			final NetworkResponse networkResponse = mockedNetworkClient.doVoid(voidMockedNetworkRequest);
			LOGGER.info(
					"Void request  with correlation id:[{}] successfully made to the mocked network with response:{}",
					request.getCorrelationId(),
					networkResponse);
			return networkResponse;
		} catch (MockedNetworkException ex) {
			LOGGER.warn("Mocked network void  with correlation id:[{}] response with error:{}",
					request.getCorrelationId(), ex.getErrorResponse(), ex);
			return ex.getErrorResponse();
		} catch (RuntimeException ex) {
			LOGGER.warn("Mocked network void  with correlation id:[{}] error:[{}]", request.getCorrelationId(),
					ex.getMessage(), ex);
			return NetworkResponse.builder().state(ERROR_STATUS).errorDetails(ex.getMessage()).build();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override public NetworkResponse doRefund(final RefundRequestMessage request) {

		final NetworkRefundRequest refundMockedNetworkRequest = MockedNetworkRequestFactory
				.createRefundRequest(request);

		try {
			final NetworkResponse networkResponse = mockedNetworkClient.doRefund(refundMockedNetworkRequest);
			LOGGER.info(
					"Refund request  with correlation id:[{}] successfully made to the mocked network with response:{}",
					request.getCorrelationId(),
					networkResponse);
			return networkResponse;
		} catch (MockedNetworkException ex) {
			LOGGER.warn("Mocked network refund  with correlation id:[{}] response with error:{}",
					request.getCorrelationId(), ex.getErrorResponse(), ex);
			return ex.getErrorResponse();
		} catch (RuntimeException ex) {
			LOGGER.warn("Mocked network refund  with correlation id:[{}] error:[{}]", request.getCorrelationId(),
					ex.getMessage(), ex);
			return NetworkResponse.builder().state(ERROR_STATUS).errorDetails(ex.getMessage()).build();
		}
	}
}
