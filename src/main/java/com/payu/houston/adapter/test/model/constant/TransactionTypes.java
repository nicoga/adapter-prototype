/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.model.constant;

/**
 * Definition of the types of transactions.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public enum TransactionTypes {

	AUTHORIZATION,

	CAPTURE,

	VOID,

	REFUND
}
