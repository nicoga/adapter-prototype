/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/17/2020
 *
 */
package com.payu.houston.adapter.test.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * Houston Test adapter application - Spring Boot application configuration class
 *
 * @author Oscar Romero (oscar.romero@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@EnableFeignClients(basePackages = { "com.payu.houston.adapter.test.client" })
@SpringBootApplication(scanBasePackages = { "com.payu.houston.adapter.test.application",
		"com.payu.houston.adapter.test.provider",
		"com.payu.houston.adapter.test.service" })
public class TestAdapterApplication {

	/**
	 * Starts the Houston Test Adapter application
	 *
	 * @param args Application parameters
	 */
	public static void main(final String[] args) {

		SpringApplication.run(TestAdapterApplication.class, args);
	}
	
}
