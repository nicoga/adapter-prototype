/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/20/2020
 */
package com.payu.houston.adapter.test.model;

import lombok.Builder;
import lombok.Data;

/**
 * Mocked network merchant definition.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@Data
@Builder
public class Merchant {

	/**
	 * Merchant API login.
	 */
	private String apiLogin;

	/**
	 * Merchant API key.
	 */
	private String apiKey;
}
