/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   09/01/2020
 */
package com.payu.houston.adapter.test.model.constant;

import com.payu.houston.adapter.queue.model.authorization.AuthorizationResponseCode;
import com.payu.houston.adapter.queue.model.capture.CaptureResponseCode;
import com.payu.houston.adapter.queue.model.refunds.RefundResponseCode;
import com.payu.houston.adapter.queue.model.voids.VoidResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * Mapper for mocked network transaction response codes.
 *
 * @author Nicolas Garcia Rey (nicolas.garcia@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
@AllArgsConstructor
@Getter
public enum TransactionResponseCodeMapper {

	CODE_0("0",
			AuthorizationResponseCode.APPROVED,
			VoidResponseCode.APPROVED,
			RefundResponseCode.APPROVED,
			CaptureResponseCode.APPROVED),

	CODE_1("-1",
			AuthorizationResponseCode.INSUFFICIENT_FUNDS,
			null,
			null,
			null),

	CODE_2("-2",
			AuthorizationResponseCode.PAYMENT_NETWORK_REJECTED,
			VoidResponseCode.PAYMENT_NETWORK_REJECTED,
			RefundResponseCode.PAYMENT_NETWORK_REJECTED,
			CaptureResponseCode.PAYMENT_NETWORK_REJECTED),

	CODE_3("-3",
			AuthorizationResponseCode.INTERNAL_PAYMENT_PROVIDER_ERROR,
			VoidResponseCode.INTERNAL_PAYMENT_PROVIDER_ERROR,
			RefundResponseCode.INTERNAL_PAYMENT_PROVIDER_ERROR,
			CaptureResponseCode.INTERNAL_PAYMENT_PROVIDER_ERROR),

	UNKNOWN("00",
			AuthorizationResponseCode.NOT_ACCEPTED_TRANSACTION,
			VoidResponseCode.NOT_ACCEPTED_TRANSACTION,
			RefundResponseCode.NOT_ACCEPTED_TRANSACTION,
			CaptureResponseCode.NOT_ACCEPTED_TRANSACTION);

	private final String mockedNetworkCode;

	private final AuthorizationResponseCode authorizationResponseCode;

	private final VoidResponseCode voidResponseCode;

	private final RefundResponseCode refundResponseCode;

	private final CaptureResponseCode captureResponseCode;

	public static TransactionResponseCodeMapper buildFromNetworkCode(final String code) {

		return Arrays.stream(values())
				.filter(codes -> codes.mockedNetworkCode.equals(code))
				.findFirst().orElse(UNKNOWN);
	}
}
