/*
 * PayU Latam - Copyright (c) 2013 - 2020
 * http://www.payu.com.co
 * Date:   08/17/2020
 *
 */
package com.payu.houston.adapter.test.application;

import com.payu.houston.adapter.queue.model.common.RequestMessage;
import com.payu.houston.adapter.test.provider.HoustonTestProviderHelper;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

/**
 * Defines a generic flow/route for Apache Camel
 *
 * @author Manuel E Vieda (manuel.vieda@payulatam.com)
 * @version 1.0
 * @since 1.0
 */
public abstract class GenericFlow extends RouteBuilder {

	/**
	 * Apache camel FROM URI pattern
	 */
	public static final String QUEUE_NAME_PATTERN = "activemq:queue:%1$s?concurrentConsumers=%2$s&maxConcurrentConsumers=%3$s";

	/**
	 * The addDelay method
	 */
	private static final String ADD_DELAY_METHOD = "addDelay";

	/**
	 * Queue name from which the request messages are taken
	 */
	private String requestQueueName;

	/**
	 * Queue name by which the response messages are sent back to engine.
	 */
	private String responseQueueName;

	/**
	 * Specify the number of concurrent consumers to create.
	 * <p>
	 * Specifying a higher value for this setting will increase the standard level of scheduled concurrent consumers at
	 * runtime: This is effectively the minimum number of concurrent consumers which will be scheduled at any given
	 * time.
	 * <p>
	 * This is a static setting; for dynamic scaling, consider specifying the "maxConcurrentConsumers" setting instead.
	 */
	private Integer concurrentConsumers;

	/**
	 * Specify the maximum number of concurrent consumers to create.
	 * <p>
	 * If this setting is higher than "concurrentConsumers", the listener container will dynamically schedule new
	 * consumers at runtime, provided that enough incoming messages are encountered. Once the load goes down again, the
	 * number of consumers will be reduced to the standard level ("concurrentConsumers") again.
	 */
	private Integer maxConcurrentConsumers;

	/**
	 * Defines the routing (Flow) in the CamelContext that will define and controls the lifecycle of the messages:
	 * <ul>
	 * <li>Read message from the queue (Configuring the name of the queue and number of consumers)</li>
	 * <li>Converts the stream (JSON Format) of the message to the appropriate Java object</li>
	 * <li>Send the message to the bean responsible of processing the information</li>
	 * <li>Converts the response into a byte-stream -(JSON Format)</li>
	 * <li>Puts the response stream into the response queue</li>
	 * </ul>
	 */
	@Override
	public void configure() throws ClassNotFoundException {

		getContext().setAllowUseOriginalMessage(false);
		getContext().setStreamCaching(false);

		fromF(QUEUE_NAME_PATTERN, requestQueueName, concurrentConsumers, maxConcurrentConsumers)
				.unmarshal().json(JsonLibrary.Jackson, getRequestMessageClass())
				.bean(HoustonTestProviderHelper.class, ADD_DELAY_METHOD)
				.bean("houstonTestProvider", getProviderHandlerMethod())
				.marshal().json(JsonLibrary.Jackson)
				.toF(QUEUE_NAME_PATTERN, responseQueueName, concurrentConsumers, maxConcurrentConsumers);
	}

	/**
	 * Returns the name of the method that handles the request message and produces the response message. This method
	 * must be defined in {@linkplain FacProvider} class
	 *
	 * @return The message handler for the given operation
	 */
	protected abstract String getProviderHandlerMethod();

	/**
	 * Returns the fully qualified name of the request message class
	 *
	 * @return The fully qualified name of the request message class
	 */
	public abstract <T extends RequestMessage> Class<T> getRequestMessageClass();

	/**
	 * Returns the request messages queue name
	 *
	 * @return The request message queue name
	 */
	public String getRequestQueueName() {

		return requestQueueName;
	}

	/**
	 * Returns the response messages queue name
	 *
	 * @return The response messages queue name
	 */
	public String getResponseQueueName() {

		return responseQueueName;
	}

	/**
	 * Returns the number of concurrent consumers used by the request listener
	 *
	 * @return The number of ocncurrent consumers used by the request listener
	 */
	public Integer getConcurrentConsumers() {

		return concurrentConsumers;
	}

	/**
	 * Returns the maximum number of concurrent consumers used by the request listener
	 *
	 * @return The maximum number of concurrent consumers used by the request listener
	 */
	public Integer getMaxConcurrentConsumers() {

		return maxConcurrentConsumers;
	}

	/**
	 * Sets the the request messages queue name
	 *
	 * @param requestQueueName the request messages queue name to set
	 */
	public void setRequestQueueName(final String requestQueueName) {

		this.requestQueueName = requestQueueName;
	}

	/**
	 * Sets the response messages queue name
	 *
	 * @param responseQueueName the response messages queue name
	 */
	public void setResponseQueueName(final String responseQueueName) {

		this.responseQueueName = responseQueueName;
	}

	/**
	 * Sets the number of concurrent consumers used by the request listener
	 *
	 * @param concurrentConsumers the number of concurrent consumers to set
	 */
	public void setConcurrentConsumers(final Integer concurrentConsumers) {

		this.concurrentConsumers = concurrentConsumers;
	}

	/**
	 * Sets the maximum number of concurrent consumers used by the request listener
	 *
	 * @param maxConcurrentConsumers the maximum number of concurrent consumers to set
	 */
	public void setMaxConcurrentConsumers(final Integer maxConcurrentConsumers) {

		this.maxConcurrentConsumers = maxConcurrentConsumers;
	}
}
