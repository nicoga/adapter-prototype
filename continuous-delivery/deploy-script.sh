#!/bin/bash
#
# PayU Latam - Copyright (c) 2013 - 2020
# http://www.payu.com.co
# Date:   08/11/2020
#
set -e

PROJECT_NAME="Houston test adapter"
MAIN_YML_FILE="test-adapter-app.yml"

echo ""
echo "================================================="
echo "Devops $PROJECT_NAME Deployment script"
echo ""

if [ $# == 0 ]; then
	SHOW_HELP=YES
elif [ $# == 1 ] && [ $1 == "--help" ]; then
	SHOW_HELP=YES
fi

# Process parameters
while [[ $# > 1 ]]
do
key="$1"

case $key in
 -a|--action)
    ACTION="$2"
    shift # past argument
    ;;
 -c|--component)
    COMPONENT="$2.yml"
    shift # past argument
    ;;
 -p|--playbook)
    PLAYBOOK_DIR="$2"
    shift # past argument
    ;;
 -h|--host)
    LIMIT_HOST="$2"
    shift # past argument
    ;;
 -v|--version)
    BUILD_VERSION="$2"
    shift # past argument
    ;;

 -b|--branch)
    BRANCH="$2"
    shift # past argument
    ;;

 -env|--environment)
    ENVIRONMENT="$2"
    shift # past argument
    ;;
  -t|--tags)
    TAGS="$2"
    shift # past argument
    ;;
 -d|--debug)
    PAYU_ANSIBLE_DEBUG="$2"
    shift # past argument
    ;;
 --check)
	USE_CHECK_MODE=YES
    ;;
 --help)
    SHOW_HELP=YES
    ;;
 --extra-vars)
    PARAM_EXTRA_VARS="$2"
    shift # past argument
    ;;

    *)
         # unknown option
    ;;
esac
shift # past argument or value
done

# Initialization of roles

if [ -z "$PLAYBOOK_DIR" ]; then
		PLAYBOOK_DIR="."
fi
if [ -z "$COMPONENT" ]; then
		COMPONENT="$MAIN_YML_FILE"
fi


# Aux variables
INVENTORY_FILE=$PLAYBOOK_DIR/inv/$ENVIRONMENT/inventory

# Set the extra vars to send to ansible
ANSIBLE_EXTRA_VARS="app_version=$BUILD_VERSION app_branch=$BRANCH"

# Set the extra vars to send to ansible
if [ "$PARAM_EXTRA_VARS" != "" ]; then
   ANSIBLE_EXTRA_VARS="$ANSIBLE_EXTRA_VARS $PARAM_EXTRA_VARS"
fi


# Configure the verbose debug option
ANSIBLE_DEBUG_OPTION=""
if [ "$PAYU_ANSIBLE_DEBUG" != "" ]; then
	ANSIBLE_DEBUG_OPTION="-$PAYU_ANSIBLE_DEBUG"
fi

# Configure the limit option
ANSIBLE_LIMIT_OPTION=""
if [ "$LIMIT_HOST" != "" ]; then
	ANSIBLE_LIMIT_OPTION="--limit $LIMIT_HOST"
fi

# Set the ansible options to use in all the commands
PAYU_ANSIBLE_OPTIONS="$ANSIBLE_LIMIT_OPTION $ANSIBLE_DEBUG_OPTION"


if ! [ -z "$USE_CHECK_MODE" ]; then
	PAYU_ANSIBLE_OPTIONS="$PAYU_ANSIBLE_OPTIONS --check "
fi


# ========================================================================================
# ========================================================================================

run_help(){
	echo "Usage: $0 [options] "
	echo "Options:"
	echo "  -a , --action   : The action to use. Can be ping, start, stop, clean, setup, deploy, full-deploy"
	echo "     actions:"
	echo "           ping        : Ping the servers defined in the selected inventory"
	echo "           setup       : Do the setup tasks"
	echo "           deploy      : Do the deploy tasks, the system should be setup first"
	echo "           start       : Starts $PROJECT_NAME, first core then web"
	echo "           stop        : Stops PayU, starting from Web tier, then core"
	echo "           clean       : Clean the servers by deleting log files, artifacts and others. Note that with 0% free space this will not work"
	echo "           full-deploy : Performs a full deploy on the system, do not start at the end."
	echo "           full-deploy-start  : Performs a full deploy on the system, start at the end. "
	echo "  -p , --playbook : The path to the Ansible Playbook directory to use."
	echo "  -v , --version  : The version of the app to deploy."
	echo "  -h , --host     : The host to apply the playbook. Depends on playbook configuration, used to limit the execution to some servers"
	echo "  -t , --tags     : The tags to execute, only with action 'test'"
	echo "  -env , --environment   : The enviroment to use. Depends on playbook configuration."
	echo "  -d , --debug    : Use the ansible verbose flags, specify vvvv's with this option"
    echo "  --extra-vars    : Use this parameter to send a json string with variables to the process. If you use this, you must send the vars: BUILD_VERSION , BRANCH "
	echo "  --check yes     : Use ansible check mode, don't perform any task on hosts"
	echo "  --help yes      : Print this information"
	echo ""
}

# If the user select to show help
if [ "$SHOW_HELP" == "YES" ]; then
   run_help
   exit 1
fi

# Method to call the ping option
run_ping(){
	echo "Ping servers"
	sudo -u ansible ansible -m ping -i $INVENTORY_FILE all

    RETVAL=$?
	return $RETVAL
}

# Method to call the start option
run_start(){
    echo "Starting $PROJECT_NAME"
    sudo -u ansible ansible-playbook -i $INVENTORY_FILE $PLAYBOOK_DIR/$COMPONENT --tags='start' --extra-vars="$ANSIBLE_EXTRA_VARS" $PAYU_ANSIBLE_OPTIONS

    RETVAL=$?
	return $RETVAL

}

# Method to call the stop option
run_stop(){
    echo "Stopping $PROJECT_NAME"
    sudo -u ansible ansible-playbook -i $INVENTORY_FILE $PLAYBOOK_DIR/$COMPONENT --tags='stop' $PAYU_ANSIBLE_OPTIONS

    RETVAL=$?
	return $RETVAL

}

# Method to call the clean option
run_clean(){
	echo "Cleaning tasks Core "
	sudo -u ansible ansible-playbook -i $INVENTORY_FILE $PLAYBOOK_DIR/$COMPONENT  --tags='clean' $PAYU_ANSIBLE_OPTIONS

    RETVAL=$?
	return $RETVAL

}

# Method to call the deploy option, use 1 parameter to select if the core or web should be deployed
run_deploy(){
    echo "Deploying $PROJECT_NAME"
    sudo -u ansible ansible-playbook -i $INVENTORY_FILE $PLAYBOOK_DIR/$COMPONENT --extra-vars="$ANSIBLE_EXTRA_VARS" --tags='deploy' $PAYU_ANSIBLE_OPTIONS

    RETVAL=$?
	return $RETVAL

}

# Method to call the deploy option, use 1 parameter to select if the core or web should be deployed
run_setup(){
    echo "Setup $PROJECT_NAME"
    sudo -u ansible ansible-playbook -i $INVENTORY_FILE $PLAYBOOK_DIR/$COMPONENT --extra-vars="$ANSIBLE_EXTRA_VARS" --tags='setup' $PAYU_ANSIBLE_OPTIONS

    RETVAL=$?
	return $RETVAL
}


# Method to call the Full Deploy option
run_full_deploy(){

    if [ $# == 1 ] && [ $1 == "start" ]; then
		SHOULD_START=YES
	fi

	echo ""
	echo "Running Setup task..."
	run_setup
	SETUP_RET_CODE=$?

	echo ""
	if [ $SETUP_RET_CODE != 0 ]; then
	  echo "*** Setup task ended with problems. Return code: $SETUP_RET_CODE"
	  return $SETUP_RET_CODE
	fi

	echo ""
	echo "Running stop task..."
	run_stop
	STOP_RET_CODE=$?

	echo ""
	if [ $STOP_RET_CODE != 0 ]; then
      	echo "*** Stop task ended with problems. Return code: $STOP_RET_CODE"
        return $STOP_RET_CODE
	fi

	echo ""
	echo "Running deploy task..."
	run_deploy
	DEP_RET_CODE=$?

	echo ""
	if [ $DEP_RET_CODE != 0 ]; then
        echo "*** Deploy task ended with problems. Return code: $DEP_RET_CODE"
        return $DEP_RET_CODE
	fi

    if [ "$SHOULD_START" == "YES" ]; then
        echo "Running the start task"
        run_start
        START_RET_CODE=$?
	    echo ""
	    if [ $START_RET_CODE != 0 ]; then
	        echo "*** Start task ended with problems. Return code: $START_RET_CODE"
	        return $START_RET_CODE
	    fi
    fi


    return 0

}


# ========================================================================================
# ========================================================================================
# This section handles all the commands use to automate the process


# If the user select to show help
if [ "$SHOW_HELP" == "YES" ]; then
   run_help
   exit 1
fi

if [ "$ACTION" == "ping" ]; then
	run_ping
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE
fi

if [ "$ACTION" == "start" ]; then
	run_start
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE
fi

if [ "$ACTION" == "stop" ]; then
	run_stop
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE
fi

if [ "$ACTION" == "restart" ]; then
	run_stop
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
        exit $RET_CODE
    fi

	run_start
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
        exit $RET_CODE
    fi

    exit 0

fi

if [ "$ACTION" == "deploy" ]; then
	run_deploy
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE
fi

if [ "$ACTION" == "setup" ]; then
	run_setup
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE
fi


if [ "$ACTION" == "clean" ]; then
    run_clean
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE
fi

if [ "$ACTION" == "full-deploy" ]; then
	echo "DEVOPS $PROJECT_NAME Full Deploy"

	run_full_deploy
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE

fi


if [ "$ACTION" == "full-deploy-start" ]; then
    echo "DEVOPS $PROJECT_NAME Full Deploy and start"

	run_full_deploy start
	RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE

fi


if [ "$ACTION" == "tag" ]; then
    echo "Executing tags on: [$COMPONENT] Tags: $TAGS"
    sudo -u ansible ansible-playbook -i $INVENTORY_FILE $PLAYBOOK_DIR/$COMPONENT.yml --extra-vars="$ANSIBLE_EXTRA_VARS" --tags="$TAGS" $PAYU_ANSIBLE_OPTIONS
    RET_CODE=$?
    echo ""
    if [ $RET_CODE != 0 ]; then
        echo " *** Task ended with problems. Return code: $RET_CODE"
        echo ""
    fi
    exit $RET_CODE
fi



echo ""
echo "Action: $ACTION"
if [[ "$COMPONENT" != "" ]]; then
   echo "Component: $COMPONENT"
fi
echo "Playbook dir: $PLAYBOOK_DIR"
echo "Environment: $ENVIRONMENT"
if [[ "$BUILD_VERSION" != "" ]]; then
   echo "Version: $BUILD_VERSION"
fi
if [[ "$BRANCH" != "" ]]; then
   echo "Branch:  $BRANCH"
fi
if [[ "$LIMIT_HOST" != "" ]]; then
   echo "Limited to host: $LIMIT_HOST"
fi
if [[ "$ANSIBLE_DEBUG_OPTION" != "" ]]; then
   echo "Ansible Debug Verbosity: $ANSIBLE_DEBUG_OPTION"
fi
if [[ "$USE_CHECK_MODE" != "" ]]; then
   echo "Ansible Check Mode: $USE_CHECK_MODE"
fi


echo ""
echo "Finished $PROJECT_NAME Deployment script execution."
echo "================================================================================"
