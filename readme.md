# Test Adapter Repository #

### Test basic information ###

* Test adapter implementation for PayU Houston Project.
* Version: **1.0.2.RC1**
* The Test adapter is an isolated project that can be created, maintained and supported independently from any other project.
* In order to communicate with external services an [ActiveMQ Queue](http://activemq.apache.org/) is used to transport incoming requests and outgoing responses.
* The supported operations are Authorization, Capture, Query, Refund and Void.
* [Acquirer Adapters Documentation](https://pagosonline.jira.com/wiki/display/HoustonProject/9-+Acquirer+Adapter+design)

### Acquirer important information ###

* The supported (relevant) operations are Authorization, Capture, Refund, Query and Void.

### Relevant Dependencies ###

* Add important dependencies information alphabetically: E.g.
* Apache-Camel - 2.17.2
* Acquirer-Model - 1.0.0
* Spring-Boot - 1.4.0.RELEASE
* Apache-Activemq - 5.13.3

### Internal Policies ###
* Use Git Flow.
 * [Git Flow Atlassian tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
 * [Vincent Driessen's original Git Flow proposal](http://nvie.com/posts/a-successful-git-branching-model/)
* [Source code format](https://pagosonline.jira.com/wiki/pages/viewpage.action?pageId=92045457)
* [Repository commits](https://pagosonline.jira.com/wiki/pages/viewpage.action?pageId=39026806)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
